<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questionnaire extends Model
{
    use SoftDeletes;

    public function coach()
    {
        return $this->belongsTo(User::class, 'coach_id');
    }

    public function quizzes()
    {
        return $this->hasMany(Quiz::class);
    }
}
