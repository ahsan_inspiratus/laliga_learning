<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->type == USER_TYPE_ADMIN) {
            $courses = Course::latest()->get();
        } else {
            $courses = Auth::user()->coach_courses()->latest()->get();
        }
        return view('courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => ['required', 'string', 'min:5'],
            'price' => ['required', 'numeric']
        ];

        if($request->image){
            $rules['image'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];
        }

        $validatedData = $request->validate($rules);

        $course = new Course;

        if($request->image) {
            $extension  = $request->image->getClientOriginalExtension();
            $photo      = date('Ymdhis') . mt_rand(11, 99) . mt_rand(11, 99)
                . '.' . $extension;
            $photo_path = $request->image->storeAs('public/course-images',
                $photo);
            $photo_path = substr($photo_path, 7);
            $course->image = $photo_path;
        }

        $course->title = $request->input('title');
        $course->description = $request->input('description');
        $course->category = $request->input('category');
        $course->price = $request->input('price');
        $course->is_active = $request->input('is_active');
        $course->save();

        return redirect()->route('admin.courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $session_id, Course $course)
    {
        // Check is paid fo the course, redirect to payment if not
        if(!$course->isPurchased()){
            // Redirect to Payment Page
//            return redirect()->route('players.payments.create', [session('session_id'), $course]);
        }

        return view('courses.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('courses.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $rules = [
            'title' => ['required', 'string', 'min:5'],
            'price' => ['required', 'numeric']
        ];

        if($request->image){
            $rules['image'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];
        }

        $validatedData = $request->validate($rules);

        if($request->image) {
            $extension  = $request->image->getClientOriginalExtension();
            $photo      = date('Ymdhis') . mt_rand(11, 99) . mt_rand(11, 99)
                . '.' . $extension;
            $photo_path = $request->image->storeAs('public/course-images',
                $photo);
            $photo_path = substr($photo_path, 7);
            $course->image = $photo_path;
        }

        $course->title = $request->input('title');
        $course->description = $request->input('description');
        $course->category = $request->input('category');
        $course->price = $request->input('price');
        $course->is_active = $request->input('is_active');
        $course->save();

        return redirect()->route('admin.courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }

    public function assignCoaches(Course $course)
    {
        $coaches = User::where('type', USER_TYPE_COACH)->get();
        $course_coaches = $course->coaches()->pluck('coach_id')->toArray();
        return view('courses.assign_coaches', compact('course', 'coaches', 'course_coaches'));
    }

    public function assignCoachesUpdate(Request $request, Course $course)
    {
        $course->coaches()->detach();
        if($request->coaches) {
            $course->coaches()->attach($request->coaches);
        }

        return redirect()->route('admin.courses.index');
    }

    public function updateSettings(Request $request, Course $course)
    {
        $course->is_live_stream_active = $request->is_live_stream_active;
        $course->live_stream_link = '';
        if($course->is_live_stream_active){
            $course->live_stream_link = $request->live_stream_link;
        }
        $course->is_zoom_meeting_active = $request->is_zoom_meeting_active;
        $course->zoom_meeting_link = '';
        if($course->is_zoom_meeting_active){
            $course->zoom_meeting_link = $request->zoom_meeting_link;
        }

        $course->save();

        return redirect()->route('coach.courses.videos.index',[$course->id]);
    }
}
