<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminsController extends Controller
{
    public function dashboard(Request $request)
    {
        return view('admins.dashboard');
    }

    public function profile(Request $request)
    {
        $user = Auth::user();
        return view('players.profile', compact('user'));
    }

    public function profileUpdate(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->input('name');
        $user->gender = $request->input('gender');
        $user->phone = $request->input('phone');
        $user->dob = $request->input('dob');
        $user->current_football_academy = $request->input('current_football_academy');
        $user->location = $request->input('location');
        $user->country = $request->input('country');
        $user->save();

        return redirect()->route('dashboard');
    }

    public function login(Request $request)
    {
        return view('admins.login');
    }
}
