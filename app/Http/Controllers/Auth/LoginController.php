<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'redirectToUserHome';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToUserHome()
    {
        if(Auth::check()){
            if(Auth::user()->type == USER_TYPE_ADMIN){
                $dashboard_route = 'admin.dashboard';
            } elseif(Auth::user()->type == USER_TYPE_COACH){
                $dashboard_route = 'coach.dashboard';
            } else {
                $session_id = session('session_id') ?? 'DUMMY';
                $dashboard_route = 'dashboard';
                return redirect()->route($dashboard_route, [$session_id]);
            }
        } else {
            $dashboard_route = 'login';
        }

        return redirect()->route($dashboard_route);
    }

    public function verifyLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');
//        dd($credentials);
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return response()->json(['result' => 'true', 'user_id' => Auth::id()]);
        }else{
            return response()->json(['result' => 'false']);
        }
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $session_id = Str::random(30);
            session(['session_id' => $session_id]);
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function doLogin(Request $request, $id)
    {
        Auth::loginUsingId($id);

        $session_id = Str::random(30);
        session(['session_id' => $session_id]);
//        $request->session()->set('session_id', $session_id);
//        dd($session_id);
        return redirect($session_id.'/dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('https://ae.laligacademy.com/online/login/');
    }
}
