<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Course;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Utilities\Payfort;

class PaymentsController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($session_id, Course $course)
    {
        if($course->isPurchased()){
            // Redirect to Course Page
            return redirect()->route('players.courses.show', [session('session_id'), $course]);
        }

        $dates = [
            'today' => Carbon::now(),
            'coming_sunday' => Carbon::parse('next sunday'),
            'day_after_1_week' => Carbon::parse('next sunday')->addWeeks(1)->subDay(1),
            'day_after_4_weeks' => Carbon::parse('next sunday')->addWeeks(4)->subDay(1),
            'day_after_8_weeks' => Carbon::parse('next sunday')->addWeeks(8)->subDay(1),
        ];
        return view('payments.create', compact('course', 'dates'));
    }

    public function checkout(Request $request, $session_id, Course $course, $package)
    {
        $dates = [
            'coming_sunday' => Carbon::parse('next sunday'),
            'day_after_1_week' => Carbon::parse('next sunday')->addWeeks(1)->subDay(1),
            'day_after_4_weeks' => Carbon::parse('next sunday')->addWeeks(4)->subDay(1),
            'day_after_8_weeks' => Carbon::parse('next sunday')->addWeeks(8)->subDay(1),
        ];

        if($package == 1) { // 4 Weeks
            $total_amount = 830;
            $item_name = $course->title . ' - ' . '4 Weeks Payment';
            $end_date = $dates['day_after_4_weeks'];
        } else if($package == 2) { // 8 Weeks
            $total_amount = 1425;
            $item_name = $course->title . ' - ' . '8 Weeks Payment';
            $end_date = $dates['day_after_8_weeks'];
        } else if($package == 3) { // 1 Week
            $total_amount = 210;
            $item_name = $course->title . ' - ' . '1 Week Payment';
            $end_date = $dates['day_after_1_week'];
        } else {
            // Redirect to payment page
            return redirect('players.payments.create', [session('session_id'), $course]);
        }

        // Save Payment Details in DB
        $payment = new Payment;
        $payment->order_details = $item_name;
        $payment->amount = $total_amount;
        $payment->start_date = $dates['coming_sunday'];
        $payment->end_date = $end_date;
        $payment->user_id = Auth::user()->id;
        $payment->course_id = $course->id;
        $payment->save();


        // Setting variables
//        session([
//            'payment_id' => $payment->id,
//            'user_email' => Auth::user()->email,
//            'user_name' => Auth::user()->name,
//            'total_amount' => $total_amount,
//            'item_name' => $item_name
//        ]);

        $payment_id = $payment->id;

        return view('payments.checkout', compact('course', 'payment_id', 'total_amount', 'item_name'));
    }

    public function redirectRoutes($action){
        if(!isset($action)) {
            echo 'Page Not Found!';
            exit;
        }
        $objFort = new Payfort();
        if($action == 'getPaymentPage') {
            $objFort->processRequest(htmlspecialchars($_REQUEST['paymentMethod'], ENT_QUOTES, 'UTF-8'));
        }
        elseif($action == 'merchantPageReturn') {
            $objFort->processMerchantPageResponse();
        }
        elseif($action == 'processResponse') {
            $objFort->processResponse();
        }
        else{
            echo 'Page Not Found!';
            exit;
        }
    }

    public function success(Request $request)
    {
//        dd($request->all());
        $payment = Payment::find($this->getPaymentIdFromMerchant($request['merchant_extra']));
        $course = null;
        if($payment) {
            $course = $payment->course;
            if(empty($payment->transaction_reference)){
                $payment->transaction_details = json_encode(request()->all());
                $payment->status = 1;
                $payment->transaction_reference = json_encode($request['fort_id']);
                $payment->save();
            }
        }
        $session_id = session('session_id');
        return view('payments.success', ['response' => $request->all(), 'course' => $course, 'session_id' => $session_id]);
    }

    public function failure(Request $request)
    {
//        dd($request->all());
        $payment = Payment::find($this->getPaymentIdFromMerchant($request['merchant_extra']));
        $course = null;
        if($payment) {
            $course = $payment->course;
            if(empty($payment->transaction_reference)){
                $payment->transaction_details = json_encode(request()->all());
                $payment->save();
            }
        }
        $session_id = session('session_id');
        return view('payments.failure', ['response' => $request->all(), 'course' => $course, 'session_id' => $session_id]);
    }

    protected function getPaymentIdFromMerchant($merchant_extra)
    {
        return (int)substr($merchant_extra, 4);
    }
}
