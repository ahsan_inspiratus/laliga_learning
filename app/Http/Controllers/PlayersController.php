<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlayersController extends Controller
{
    public function index(Request $request)
    {
        $players = User::where('type', USER_TYPE_PLAYER)->paginate(20);

        return view('players.index', compact('players'));
    }

    public function exportAll(Request $request)
    {
        $players = User::where('type', USER_TYPE_PLAYER)->latest()->get();

        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="players-all-export-' . date('Y-m-d-h-i') . '.csv";');

        // loop over the input array
        $data = "S No., Name, Email, Phone, Gender, DOB, Country, Registered At, Email Verified, Already Paid\n";
        $ii = 1;
        foreach ($players as $player) {
            $row   = array();
            $row[] = "\"" . $ii++ . "\"";
            $row[] = "\"" . $player->name . "\"";
            $row[] = "\"" . $player->email . "\"";
            $row[] = "\"" . $player->phone . "\"";
            if(isset($player->gender)) {
                $row[] = "\"" . $player->gender . "\"";
            } else {
                $row[] = "\"-\"";
            }
            if(isset($player->dob)) {
                $row[] = "\"" . \Carbon\Carbon::parse($player->dob)->format('d/m/Y') . "\"";
            } else {
                $row[] = "\"-\"";
            }
            $row[] = "\"" . $player->country . "\"";
            $row[] = "\"" . \Carbon\Carbon::parse($player->created_at)
                    ->format('d/m/Y h:ia') . "\"";
            if ( ! empty($player->email_verified_at)){
                $row[] = "\"Yes\"";
            }else{
                $row[] = "\"No\"";
            }
            if ( ! empty($player->already_paid)){
                $row[] = "\"Yes\"";
            }else{
                $row[] = "\"No\"";
            }
            $row[] = ";\n";
            $data .= implode(',', $row);
        }
        echo $data;exit();
    }

    public function updatePaymentStatus(Request $request, User $player)
    {
        $player->already_paid = $request->already_paid;
        $player->save();

        return response()->json(['success' => true]);
    }

    public function dashboard(Request $request)
    {
//        if(Auth::user()->hasVerifiedEmail()) {
            if(empty(Auth::user()->dob)){
                return redirect()->route('players.profile');
            }

            $active_courses = Course::where('is_active', STATUS_ENABLED)->latest()->get();
            $coming_soon_courses = Course::where('is_active', STATUS_DISABLED)->latest()->get();

            return view('players.dashboard', compact('active_courses', 'coming_soon_courses'));
//        } else {
//            return redirect()->route('verification.notice');
//        }
    }

    public function profile(Request $request)
    {
        $user = Auth::user();
        return view('players.profile', compact('user'));
    }

    public function profileUpdate(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->input('name');
        $user->gender = $request->input('gender');
        $user->phone = $request->input('phone');
        $user->dob = $request->input('dob');
        $user->current_football_academy = $request->input('current_football_academy');
        $user->location = $request->input('location');
        $user->country = $request->input('country');
        $user->save();

        return redirect()->route('dashboard', [session('session_id')]);
    }

    function schedule(Request $request, $session_id)
    {
        return view('players.schedule');
    }

    function termsConditions(Request $request, $session_id)
    {
        return view('players.terms_conditions');
    }

}
