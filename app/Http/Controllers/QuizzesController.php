<?php

namespace App\Http\Controllers;

use App\Course;
use App\Questionnaire;
use App\Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class QuizzesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Course $course, Questionnaire $questionnaire)
    {
        return view('quizzes.index', compact('course', 'questionnaire'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Course $course, Questionnaire $questionnaire)
    {
        return view('quizzes.create', compact('course', 'questionnaire'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Course $course, Questionnaire $questionnaire)
    {
        $rules = [];
        if($request->media_type != 0 && $request->media) {
            if ($request->media_type == MEDIA_TYPE_IMAGE) {
                $rules['media'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];
            } elseif ($request->media_type == MEDIA_TYPE_VIDEO) {
                $rules['media'] = ['mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4'];
            }
        }

        if($rules) {
            $validatedData = $request->validate($rules);
        }

        $quiz = new Quiz;

        if($request->media_type != 0 && $request->media) {
            $extension     = $request->media->getClientOriginalExtension();
            $media         = date('Ymdhis') . mt_rand(11, 99) . mt_rand(11, 99) . '.' . $extension;
            if ($request->media_type == MEDIA_TYPE_IMAGE) {
                $quiz->media_type = MEDIA_TYPE_IMAGE;
                $media_path = $request->media->storeAs('public/quizzes-media/images/', $media);
            } elseif ($request->media_type == MEDIA_TYPE_VIDEO) {
                $quiz->media_type = MEDIA_TYPE_VIDEO;
                $media_path = $request->media->storeAs('public/quizzes-media/videos/', $media);
            }
            $media_path  = substr($media_path, 7);
            $quiz->media = Storage::url($media_path);
        }

        $quiz->question = $request->question;
        $quiz->type = $request->type;
        $quiz->options = json_encode($request->options);
        $quiz->answer = $request->answer;
        $quiz->active_descriptive_field = $request->active_descriptive_field;
        $quiz->questionnaire_id = $questionnaire->id;

        $quiz->save();

        return redirect()->route('coach.courses.questionnaires.quizzes.index', compact('course', 'questionnaire'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course, Questionnaire $questionnaire, Quiz $quiz)
    {
        return view('quizzes.edit', compact('course', 'questionnaire', 'quiz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course, Questionnaire $questionnaire, Quiz $quiz)
    {
        $rules = [];
        if($request->media_type != 0 && $request->media) {
            if ($request->media_type == MEDIA_TYPE_IMAGE) {
                $rules['media'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];
            } elseif ($request->media_type == MEDIA_TYPE_VIDEO) {
                $rules['media'] = ['mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4'];
            }
        } else {
            $quiz->media = 0;
            $quiz->media_type = 0;
        }

        if($rules) {
            $validatedData = $request->validate($rules);
        }

        if($request->media_type != 0 && $request->media) {
            $extension     = $request->media->getClientOriginalExtension();
            $media         = date('Ymdhis') . mt_rand(11, 99) . mt_rand(11, 99) . '.' . $extension;
            if ($request->media_type == MEDIA_TYPE_IMAGE) {
                $quiz->media_type = MEDIA_TYPE_IMAGE;
                $media_path = $request->media->storeAs('public/quizzes-media/images/', $media);
            } elseif ($request->media_type == MEDIA_TYPE_VIDEO) {
                $quiz->media_type = MEDIA_TYPE_VIDEO;
                $media_path = $request->media->storeAs('public/quizzes-media/videos/', $media);
            }
            $media_path  = substr($media_path, 7);
            $quiz->media = Storage::url($media_path);
        }

        $quiz->question = $request->question;
        $quiz->type = $request->type;
        $quiz->options = json_encode($request->options);
        $quiz->answer = $request->answer;
        $quiz->active_descriptive_field = $request->active_descriptive_field;

        $quiz->save();

        return redirect()->route('coach.courses.questionnaires.quizzes.index', compact('course', 'questionnaire'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Course $course, Questionnaire $questionnaire, Quiz $quiz)
    {
        if($quiz->questionnaire_id == $questionnaire->id){
            $quiz->delete();
        }
        return redirect()->route('coach.courses.questionnaires.quizzes.index', compact('course', 'questionnaire'));
    }
}
