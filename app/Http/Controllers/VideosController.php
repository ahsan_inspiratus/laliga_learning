<?php

namespace App\Http\Controllers;

use App\Course;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Course $course)
    {
        return view('videos.index', compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Course $course)
    {
        return view('videos.create', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Course $course)
    {
        $rules = [
            'title' => ['required', 'string', 'min:5'],
        ];

        if($request->type == VIDEO_TYPE_UPLOAD && $request->file){
            $rules['file'] = ['mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4'];
        }elseif($request->type == VIDEO_TYPE_IMAGE && $request->file){
            $rules['image'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];
        }

        $validatedData = $request->validate($rules);

        $video = new Video;

        if($request->type == VIDEO_TYPE_UPLOAD && $request->file){
            $extension  = $request->file->getClientOriginalExtension();
            $folder_name = str_pad($course->id, 4,'0', STR_PAD_LEFT);
            $video_name  = $folder_name. '_' . mt_rand(11, 99) . mt_rand(11, 99)
                . '.' . $extension;
            $video_link = $request->file->storeAs('public/course-videos/'.$folder_name,
                $video_name);
            $video_link = substr($video_link, 7);
            $video->link = Storage::url($video_link);
        } elseif($request->type == VIDEO_TYPE_IMAGE && $request->file){
            $extension  = $request->file->getClientOriginalExtension();
            $folder_name = str_pad($course->id, 4,'0', STR_PAD_LEFT);
            $video_name  = $folder_name. '_' . mt_rand(11, 99) . mt_rand(11, 99)
                . '.' . $extension;
            $video_link = $request->file->storeAs('public/course-videos/'.$folder_name,
                $video_name);
            $video_link = substr($video_link, 7);
            $video->link = Storage::url($video_link);
        } else {
//            $parts = parse_url($request->input('link'));
//            if(isset($parts['query'])) {
//                parse_str($parts['query'], $query);
//                if (isset($query['v'])) {
//                    $video->link = 'https://www.youtube.com/embed/'
//                        . $query['v'];
//                }
//            }
            $video->link = $request->input('link');
        }

        $video->title = $request->input('title');
        $video->description = $request->input('description');
        $video->type = $request->input('type');
        $video->length = $request->input('length');
        $video->sort_order = $request->input('sort_order');
        $video->coach_id = Auth::user()->id;
        $video->course_id = $course->id;
        $video->save();

        return redirect()->route('coach.courses.videos.index', [$course->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $session_id, Course $course, Video $video)
    {
        return view('videos.show', compact('course', 'video'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course, Video $video)
    {
        return view('videos.edit', compact('course', 'video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course, Video $video)
    {
        $rules = [
            'title' => ['required', 'string', 'min:5'],
        ];

        if($request->type == VIDEO_TYPE_UPLOAD && $request->file){
            $rules['file'] = ['mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4'];
        }elseif($request->type == VIDEO_TYPE_IMAGE && $request->file){
            $rules['image'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];
        }

        $validatedData = $request->validate($rules);

        if($request->type == VIDEO_TYPE_UPLOAD && $request->file){
            $extension  = $request->file->getClientOriginalExtension();
            $folder_name = str_pad($course->id, 4,'0', STR_PAD_LEFT);
            $video_name  = $folder_name. '_' . mt_rand(11, 99) . mt_rand(11, 99)
                . '.' . $extension;
            $video_link = $request->file->storeAs('public/course-videos/'.$folder_name,
                $video_name);
            $video_link = substr($video_link, 7);
            $video->link = Storage::url($video_link);
        } elseif($request->type == VIDEO_TYPE_IMAGE && $request->file){
            $extension  = $request->file->getClientOriginalExtension();
            $folder_name = str_pad($course->id, 4,'0', STR_PAD_LEFT);
            $video_name      = $folder_name. '_' . mt_rand(11, 99) . mt_rand(11, 99)
                    . '.' . $extension;
            $video_link = $request->file->storeAs('public/course-videos/'.$folder_name,
                $video_name);
            $video_link = substr($video_link, 7);
            $video->link = Storage::url($video_link);
        }else {
//            $parts = parse_url($request->input('link'));
//            if(isset($parts['query'])) {
//                parse_str($parts['query'], $query);
//                if (isset($query['v'])) {
//                    $video->link = 'https://www.youtube.com/embed/'
//                        . $query['v'];
//                }
//            }
            $video->link = $request->input('link');
        }

        $video->title = $request->input('title');
        $video->description = $request->input('description');
        $video->type = $request->input('type');
        $video->length = $request->input('length');
        $video->sort_order = $request->input('sort_order');
//        $video->coach_id = Auth::user()->id;
//        $video->course_id = $course->id;
        $video->save();

        return redirect()->route('coach.courses.videos.index', [$course->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //
    }

    public function like(Request $request, $session_id, Course $course, Video $video)
    {
        if($course->id == $video->course_id && Auth::check()){
            if(!Auth::user()->likedVideos()->where('video_id', $video->id)->exists()) {
                Auth::user()->likedVideos()->attach([$video->id]);
            }
            return response()->json(array('success' => true));
        }
        return response()->json(array('success' => false));
    }
}
