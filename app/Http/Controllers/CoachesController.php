<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class CoachesController extends Controller
{
    public function dashboard(Request $request)
    {

            return view('coaches.dashboard');

    }

    public function profile(Request $request)
    {
        $coach = Auth::user();
        return view('coaches.profile', compact('coach'));
    }

    public function profileUpdate(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->input('name');
        $user->gender = $request->input('gender');
        $user->phone = $request->input('phone');
        $user->dob = $request->input('dob');
        $user->current_football_academy = $request->input('current_football_academy');
        $user->location = $request->input('location');
        $user->country = $request->input('country');
        $user->save();

        return redirect()->route('coaches.dashboard');
    }

    public function index(Request $request)
    {
        $coaches = User::where('type', USER_TYPE_COACH)->latest()->get();
        return view('admins.coaches.index', compact('coaches'));
    }

    public function create(Request $request)
    {
        return view('admins.coaches.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];

        if($request->photo){
            $rules['photo'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];
        }

        $validatedData = $request->validate($rules);

        $coach = new User;

        if($request->photo) {
            $extension  = $request->photo->getClientOriginalExtension();
            $photo      = date('Ymdhis') . mt_rand(11, 99) . mt_rand(11, 99)
                . '.' . $extension;
            $photo_path = $request->photo->storeAs('public/coach-photos',
                $photo);
            $photo_path = substr($photo_path, 7);
            $coach->photo = $photo_path;
        }

        $coach->name = $request->input('name');
        $coach->email = $request->input('email');
        $coach->password = bcrypt($request->input('password'));
        $coach->bio = $request->input('bio');
        $coach->email_verified_at = Carbon::now();
        $coach->type = USER_TYPE_COACH;
        $coach->save();

        return redirect()->route('admin.coaches.index');
    }

    public function edit(Request $request, User $coach)
    {
        return view('admins.coaches.edit', compact('coach'));
    }

    public function update(Request $request, User $coach)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($coach->id)],
        ];

        if($request->password){
            $rules['password'] = ['required', 'string', 'min:8', 'confirmed'];
        }

        if($request->photo){
            $rules['photo'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];
        }

        $validatedData = $request->validate($rules);

        if($request->photo) {
            $extension  = $request->photo->getClientOriginalExtension();
            $photo      = date('Ymdhis') . mt_rand(11, 99) . mt_rand(11, 99)
                . '.' . $extension;
            $photo_path = $request->photo->storeAs('public/coach-photos',
                $photo);
            $photo_path = substr($photo_path, 7);
            $coach->photo = $photo_path;
        }

        $coach->name = $request->input('name');
//        $coach->email = $request->input('email');
        if($request->password) {
            $coach->password = bcrypt($request->input('password'));
        }
        $coach->bio = $request->input('bio');
//        $coach->email_verified_at = Carbon::now();
//        $coach->type = USER_TYPE_COACH;
        $coach->save();
        $coach->touch();

        return redirect()->route('admin.coaches.index');
    }
}
