<?php

namespace App\Http\Controllers;

use App\Course;
use App\Upload;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UploadsController extends Controller
{
    public function index(Request $request, Course $course, Video $video)
    {
        return view('videos.uploads', compact('course','video'));
    }

    public function store(Request $request, $session_id, Course $course, Video $video)
    {
        $validatedData = $request->validate([
            'file' => ['required', 'file', 'max:51200', 'mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4']
        ]);

        $upload = new Upload;

        $extension  = $request->file->getClientOriginalExtension();
        $folder_name = str_pad($course->id, 4,'0', STR_PAD_LEFT).'_'.str_pad($video->id, 4,'0', STR_PAD_LEFT);
        $video_name  = $folder_name. '_' . mt_rand(11, 99) . mt_rand(11, 99)
            . '.' . $extension;
        $video_link = $request->file->storeAs('public/video-submissions/'.$folder_name,
            $video_name);
        $video_link = substr($video_link, 7);
        $upload->link = Storage::url($video_link);

        $upload->user_id = Auth::user()->id;
        $upload->video_id = $video->id;
        $upload->save();

        return redirect()->route('players.courses.videos.show', [session('session_id'), $course->id, $video->id,'success=true']);
    }
}
