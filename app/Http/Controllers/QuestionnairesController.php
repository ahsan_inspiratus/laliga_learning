<?php

namespace App\Http\Controllers;

use App\Course;
use App\Questionnaire;
use App\QuestionnaireSubmission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionnairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Course $course)
    {
        return view('questionnaires.index', compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Course $course)
    {
        return view('questionnaires.create', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Course $course)
    {
        $questionnaire = new Questionnaire();
        $questionnaire->title = $request->title;
        $questionnaire->description = $request->description;
        $questionnaire->coach_id = Auth::user()->id;
        $questionnaire->course_id = $course->id;
        $questionnaire->save();

        return redirect()->route('coach.courses.questionnaires.index', compact('course'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course, Questionnaire $questionnaire)
    {
        return view('questionnaires.edit', compact('course', 'questionnaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course,  Questionnaire $questionnaire)
    {
        $questionnaire->title = $request->title;
        $questionnaire->description = $request->description;
        $questionnaire->save();

        return redirect()->route('coach.courses.questionnaires.index', compact('course'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Course $course, Questionnaire $questionnaire)
    {
        $questionnaire->delete();
        $questionnaire->quizzes()->delete();
        return redirect()->route('coach.courses.questionnaires.index', compact('course'));
    }

    public function playersIndex($session_id, Course $course)
    {
        return view('questionnaires.players_index', compact('course'));
    }

    public function playersShow($session_id, Course $course, Questionnaire $questionnaire)
    {
        $submission = QuestionnaireSubmission::where('questionnaire_id', $questionnaire->id)
                        ->where('user_id', Auth::user()->id)->first();
        $answers = false;
        if(!$submission){
            $submission = false;
        } else {
            $answers = json_decode($submission->answers,true);
        }
        return view('questionnaires.players_show', compact('course', 'questionnaire', 'submission', 'answers'));
    }

    function playersStore(Request $request, $session_id, Course $course, Questionnaire $questionnaire)
    {
        $submission = QuestionnaireSubmission::where('questionnaire_id',
            $questionnaire->id)->where('user_id', Auth::user()->id)->first();

        if ( ! $submission) {
            $answers = [];
            foreach ($questionnaire->quizzes as $quiz) {
                $answers['quiz_id_' . $quiz->id] = $_POST['quiz_id_' . $quiz->id];
                if(isset($_POST['descriptive_field_' . $quiz->id])){
                    $answers['descriptive_field_' . $quiz->id] = $_POST['descriptive_field_' . $quiz->id];
                }
            }

            $submission                   = new QuestionnaireSubmission;
            $submission->answers          = json_encode($answers);
            $submission->user_id          = Auth::user()->id;
            $submission->questionnaire_id = $questionnaire->id;
            $submission->save();
        }

        return redirect()->route('players.courses.questionnaires.show', [session('session_id'), $course, $questionnaire]);
    }

    public function submissionsIndex(Request $request, Course $course, Questionnaire $questionnaire)
    {
        $submissions = QuestionnaireSubmission::where('questionnaire_id',$questionnaire->id)->get();
        $total_quizzes = $questionnaire->quizzes->count();

        return view('questionnaires.submissions_index', compact('submissions', 'course', 'questionnaire', 'total_quizzes'));
    }

    public function submissionsShow(Request $request, Course $course, Questionnaire $questionnaire, QuestionnaireSubmission $submission)
    {
        $answers = json_decode($submission->answers,true);
        return view('questionnaires.players_show', compact('course', 'questionnaire', 'submission', 'answers'));
    }
}
