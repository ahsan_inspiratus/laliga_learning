<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            // Change Redirect To

            if(Auth::user()->type == USER_TYPE_ADMIN){
                $dashboard_route = 'admin.dashboard';
            } elseif(Auth::user()->type == USER_TYPE_COACH){
                $dashboard_route = 'coach.dashboard';
            } else {
                $session_id = session('session_id') ?? 'DUMMY';
                $dashboard_route = 'dashboard';
                return redirect()->route($dashboard_route, [$session_id]);
            }

            return redirect()->route($dashboard_route);
//            return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
