<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Player
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        if (Auth::check() && Auth::user()->type==USER_TYPE_PLAYER)
        {
            if(session('session_id') == $request->session_id
                || in_array(\Route::currentRouteName(), ['players.payments.success', 'players.payments.failure'])){
                return $next($request);
            } else {
//                Auth::logout();
//                return redirect('https://ae.laligacademy.com/online/login/');
            }
        }

        return redirect()->route('login');
    }
}
