<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function coaches()
    {
        return $this->belongsToMany(User::class, 'coach_course', 'course_id', 'coach_id');
    }

    public function videos()
    {
        return $this->hasMany(Video::class)->orderBy('sort_order');
    }

    public function questionnaires()
    {
        return $this->hasMany(Questionnaire::class);
    }

    public function isPurchased()
    {
        $course_id = $this->id;
        $user_id = Auth::user()->id;

        $found = Payment::where('course_id', $course_id)
                ->where('user_id', $user_id)
                ->where('status', STATUS_ENABLED)
                ->where('end_date', '>', Carbon::now()->toDateString())
                ->count();

        if( $found > 0 || Auth::user()->already_paid){
            return true;
        }
        return false;
    }
}
