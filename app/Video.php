<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function coach()
    {
        return $this->belongsTo(User::class, 'coach_id');
    }

    public function likedBy()
    {
        return $this->belongsToMany(User::class, 'user_video', 'video_id', 'user_id');
    }

    public function uploads()
    {
        return $this->hasMany(Upload::class);
    }
}
