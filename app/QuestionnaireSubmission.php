<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireSubmission extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class);
    }

    public function getScore()
    {
        $questionnaire = $this->questionnaire;
        $answers = json_decode($this->answers, true);
        $score = 0;
        foreach($questionnaire->quizzes as $quiz){
            if(isset($answers['quiz_id_'.$quiz->id]) && $answers['quiz_id_'.$quiz->id] == $quiz->answer){
                $score++;
            }
        }
        return $score;
    }
}
