<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::group(['middleware' => 'guest'], function() {
    Route::get('/', function(){
        return redirect('https://ae.laligacademy.com/online/');
    });

//});


//Auth::routes(['verify' => true]);
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('login', function(){
    return redirect('https://ae.laligacademy.com/online/login/');
});

//Route::get('/live-stream', function(){
//    return view('players.live_stream');
//});
Route::get('/redirectToUserHome', 'Auth\LoginController@redirectToUserHome');


Route::get('payments/redirectTo/{action}', 'PaymentsController@redirectRoutes');
Route::post('payments/redirectTo/{action}', 'PaymentsController@redirectRoutes')->name('players.payments.redirect_routes');

Route::name('coach.')->prefix('coach/')->group(function() {
    Route::group(['middleware' => 'guest'], function() {
//        Route::get('/', function () {
//            return redirect()->route('coach.login');
//        });
//        Route::get('/login', function () {
//            return view('auth.login');
//        })->name('login');
    });
    Route::group(['middleware' => 'coach'], function() {
        Route::get( '/dashboard', 'CoachesController@dashboard')
            ->name('dashboard');
        Route::get('/profile', 'CoachesController@profile')->name('profile');
        Route::post('/profile', 'CoachesController@profileUpdate')
            ->name('profile_update');

        Route::get('courses', 'CoursesController@index')->name('courses.index');

        Route::get('players', 'PlayersController@index')->name('players.index');

        Route::name('courses.')->group(function() {
            Route::resource('courses/{course}/videos', 'VideosController');
            Route::resource('courses/{course}/questionnaires', 'QuestionnairesController');
            Route::name('questionnaires.')->group(function() {
                Route::resource('courses/{course}/questionnaires/{questionnaire}/quizzes',
                    'QuizzesController');

                Route::get('courses/{course}/questionnaires/{questionnaire}/submissions/',
                    'QuestionnairesController@submissionsIndex')->name('submissions.index');

                Route::get('courses/{course}/questionnaires/{questionnaire}/submissions/{submission}',
                    'QuestionnairesController@submissionsShow')->name('submissions.show');
            });
            Route::post('courses/{course}/update_settings', 'CoursesController@updateSettings')->name('update_settings');

            Route::get('courses/{course}/videos/{video}/uploads', 'UploadsController@index')->name('videos.uploads.index');

        });
    });
});
Route::get('/gtK5fZS15jkj5QT8jV5l7jY3uJk0UFba4qnYtzy0', 'AdminsController@login');
Route::get('7jY3uJk0UFba4qnY', function(){
    http_response_code(404);
    die();
})->name('login');
Route::post('7jY3uJk0UFba4qnY', 'Auth\LoginController@login');
Route::name('admin.')->prefix('admin/')->group(function() {
    Route::group(['middleware' => 'guest'], function() {
//        Route::get('/', function () {
//            return redirect()->route('admin.login');
//        });
//        Route::get('/login', function () {
//            return view('auth.login');
//        })->name('login');

    });
    Route::group(['middleware' => 'admin'], function() {
        Route::get( '/dashboard', 'AdminsController@dashboard')
            ->name('dashboard');
        Route::get('/profile', 'AdminsController@profile')
            ->name('profile');
        Route::post('/profile', 'AdminsController@profileUpdate')
            ->name('profile_update');

        Route::get('coaches', 'CoachesController@index')->name('coaches.index');
        Route::get('coaches/create', 'CoachesController@create')->name('coaches.create');
        Route::post('coaches', 'CoachesController@store')->name('coaches.store');
        Route::get('coaches/{coach}/edit', 'CoachesController@edit')->name('coaches.edit');
        Route::match(['put', 'patch'],'coaches/{coach}', 'CoachesController@update')->name('coaches.update');

        Route::resource('courses', 'CoursesController');
        Route::get('courses/{course}/assign_coaches', 'CoursesController@assignCoaches')->name('courses.assign_coaches');
        Route::post('courses/{course}/assign_coaches', 'CoursesController@assignCoachesUpdate')->name('courses.assign_coaches_update');

        Route::get('players', 'PlayersController@index')->name('players.index');
        Route::get('players/export', 'PlayersController@exportAll')->name('players.export_all');
        Route::post('players/{player}/updatePaymentStatus', 'PlayersController@updatePaymentStatus')->name('players.update_payment_status');
    });
});

Route::post('/auth/verifyLogin', 'Auth\LoginController@verifyLogin');
Route::get('/auth/doLogin/{id}', 'Auth\LoginController@doLogin');

Route::group(['middleware' => 'player'], function(){

    Route::get('/{session_id}/schedule', 'PlayersController@schedule')->name('schedule');
    Route::get('/{session_id}/terms-conditions', 'PlayersController@termsConditions')->name('terms_conditions');

    Route::get( '/{session_id}/dashboard', 'PlayersController@dashboard')
        ->name('dashboard');

    Route::name('players.')->group(function() {
        Route::get('/{session_id}/profile', 'PlayersController@profile')
            ->name('profile');
        Route::post('/{session_id}/profile', 'PlayersController@profileUpdate')
            ->name('profile_update');
        Route::get('/{session_id}/courses/{course}', 'CoursesController@show')->name('courses.show');
        Route::get('/{session_id}/courses/{course}/videos/{video}', 'VideosController@show')->name('courses.videos.show');
        Route::post('/{session_id}/courses/{course}/videos/{video}/like', 'VideosController@like')->name('courses.videos.like');
        Route::post('/{session_id}/courses/{course}/videos/{video}/uploads', 'UploadsController@store')->name('courses.videos.upload');

        Route::get('/{session_id}/courses/{course}/questionnaires', 'QuestionnairesController@playersIndex')->name('courses.questionnaires.index');
        Route::get('/{session_id}/courses/{course}/questionnaires/{questionnaire}', 'QuestionnairesController@playersShow')->name('courses.questionnaires.show');
        Route::post('/{session_id}/courses/{course}/questionnaires/{questionnaire}', 'QuestionnairesController@playersStore')->name('courses.questionnaires.store');

        Route::get('/payments/success', 'PaymentsController@success')->name('payments.success');
        Route::get('/payments/failure', 'PaymentsController@failure')->name('payments.failure');

        Route::get('/{session_id}/payments/{course}', 'PaymentsController@create')->name('payments.create');
        Route::post('/{session_id}/payments/{course}/checkout/{package}', 'PaymentsController@checkout')->name('payments.checkout');


    });

});

Route::get('/lout', function(){
    Auth::logout();
});

Route::get('/clear-cache', function() {
    Artisan::call('view:clear');
    return "Cache is cleared";
});
