<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->id();
            $table->string('question');
            $table->tinyInteger('type')->default(QUESTIONNAIRE_TYPE_MCQS);
            $table->text('options')->nullable();
            $table->string('answer')->nullable();
            $table->tinyInteger('media_type')->default(0);
            $table->string('media')->nullable();
            $table->integer('questionnaire_id');
            $table->tinyInteger('active_descriptive_field')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }
}
