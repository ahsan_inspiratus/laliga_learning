@csrf
<div class="control-group">
    <label for="title">Title</label>
    <input type="text" name="title" value="{{ old('title', $course->title ?? '') }}" required class="form-control">
</div>

<div class="control-group">
    <label for="description">Description</label>
    <textarea name="description" class="form-control">{{ old('description', $course->description ?? '') }}</textarea>
</div>

<div class="control-group">
    <label for="image">Image</label>
    <input type="file" name="image" class="form-control" accept="image/*" >
    @if(!empty($course->image))
        <strong>Current Image</strong><br>
        <img src="{{ Storage::url($course->image) }}" alt="" style="max-width: 200px; max-height: 150px">
    @endif
</div>

<div class="control-group">
    <label for="category">Age Category</label>
    <input type="text" name="category" value="{{ old('category', $course->category ?? '') }}" required class="form-control">
</div>

<div class="control-group">
    <label for="price">Price</label>
    <input type="number" name="price" min="0" value="{{ old('price', $course->price ?? '') }}" required class="form-control">
</div>

<div class="control-group">
    <label for="is_active">Status</label>
    <input type="radio" name="is_active" required value="0" @if(old('is_active', $course->is_active ?? '') == 0) checked @endif> Disabled
    <input type="radio" name="is_active" required value="1" @if(old('is_active', $course->is_active ?? '') == 1) checked @endif> Enabled
</div>
