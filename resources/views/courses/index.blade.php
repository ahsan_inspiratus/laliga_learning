@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            @if(Auth::user()->type == USER_TYPE_ADMIN)
                <h2 class="d-inline-block">All Courses</h2>
                <a href="{{ route('admin.courses.create') }}" class="btn btn-primary float-right">+ Add Course</a>
            @else
                <h2 class="d-inline-block">Assigned Courses</h2>
            @endif
        </div>
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>Thumbnail</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Updated On</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($courses as $course)
                <tr>
                    <td>{{ $course->id }}</td>
                    <td><img src="{{ Storage::url($course->image) }}" alt="" style="max-width: 150px; max-height: 100px;"></td>
                    <td>{{ $course->title }}</td>
                    <td>{{ $course->category }}</td>
                    <td>{{ $course->price }} AED</td>
                    <td>{{ $course->is_active ? 'Active' : 'Inactive' }}</td>
                    <td>{{ $course->updated_at }}</td>
                    <td>
                        @if(Auth::user()->type == USER_TYPE_ADMIN)
                            <a class="btn btn-info btn-sm" href="{{ route('admin.courses.edit', [$course->id]) }}">Edit</a>
                            <a class="btn btn-info btn-sm" href="{{ route('admin.courses.assign_coaches', [$course->id]) }}">Assign Coaches</a>
                        @else
                            <a class="btn btn-info btn-sm" href="{{ route('coach.courses.videos.index', [$course->id]) }}">Videos</a>
                            <a class="btn btn-info btn-sm" href="{{ route('coach.courses.questionnaires.index', [$course->id]) }}">Questionnaires</a>
                        @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            {{--<ul class="pagination">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
            </ul>--}}
        </div>
    </div>
@endsection
