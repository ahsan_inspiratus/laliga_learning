@extends('layouts.app')

@section('content')
@include('partials._header')
    <section class="dashboard-layout bg-grey">
        {{--<header class="dashboard-header">
            <div class="container">
                <div class="header-content">
                    <h4 class="title">{{ $course->title }}</h4>
                </div>
            </div>
        </header>--}}

        <section class="dashboard-body">
            <div class="container">

                <h2 class="form-title text-center">Training Videos</h2>
                @if($course->is_live_stream_active)
                <div class="text-center mb-20">
                    <a href="{{ $course->live_stream_link }}" class="btn --btn-primary">Live Streaming</a>
                </div>
                @endif
                @if($course->is_zoom_meeting_active)
                <div class="text-center mb-40">
                    <a href="{{ $course->zoom_meeting_link }}" class="btn --btn-zoom">Join Zoom Meeting</a>
                </div>
                @endif

                @if($course->questionnaires->count() > 0)
                <div class="text-center mb-40">
                    <a href="{{ route('players.courses.questionnaires.index', [session('session_id'), $course->id]) }}" class="btn --btn-primary">Quiz Section</a>
                </div>
                @endif
                <!-- <div class="iframe-container" style="overflow: hidden; padding-top: 56.25%; position: relative;">
        <iframe allow="microphone; camera" style="border: 0; height: 100%; left: 0; position: absolute; top: 0; width: 100%;" src="https://success.zoom.us/wc/71587252556/join?track_id=&jmf_code=&meeting_result=&wpk=wcpk8ea140df1aec8fb043ff5ee88df109da" frameborder="0"></iframe>

    </div> -->






                <div class="row">
                    @foreach($course->videos as $video)
                    <div class="col-sm-6">
                        <a href="{{ route('players.courses.videos.show', [session('session_id'), str_pad($course->id, 4, '0', STR_PAD_LEFT), str_pad($video->id, 4, '0', STR_PAD_LEFT)]) }}" class="box --small-course-box">
                            @if($video->type == VIDEO_TYPE_IMAGE)
                                <figure style="background-image: url('{{ empty($video->link) ? url("https://img.youtube.com/vi/pWGgjNQcJns/maxresdefault.jpg") : $video->link }}');"></figure>
                            @else
                            <figure style="background-image: url('{{ empty($course->image) ? url("https://img.youtube.com/vi/pWGgjNQcJns/maxresdefault.jpg") : Storage::url($course->image) }}');"></figure>
                            @endif

                            <p class="title">{{ $video->title }}</p>

                            <p class="desc">{{ $video->description }}</p>

                            <p class="date"><!-- {{ $video->length }} - -->22/4/2020</p>
                        </a>
                    </div>
                    @endforeach
                </div>

                {{-- <div class="row align-items-center">
                    <div class="col-12">
                        <p class="maintitle">Course Detail</p>

                        <p class="desc mb-40">{{ $course->description }}</p>
                    </div>
                </div>


                <div class="row align-items-center">
                    <div class="col-8">
                        <p class="maintitle">Coaches</p>
                    </div>

                    <!-- <div class="col-4">
                        <a href="#" class="btn --btn-link float-right">See All</a>
                    </div> -->
                </div>

                <div class="d-flex">
                    @foreach($course->coaches as $coach)
                    <a href="#" class="box --coach-box">
                        <figure style="background-image: url( {{ empty($coach->photo) ? asset('/assets-web/images/dummy-coach.jpg') : Storage::url($coach->photo) }});"></figure>

                        <p class="name">{{ $coach->name }}</p>
                    </a>
                    @endforeach
                </div>


                <div class="row align-items-center">
                    <div class="col-8">
                        <p class="maintitle">Course Videos</p>
                    </div>

                    <div class="col-4">
                        <a href="#" class="btn --btn-link float-right">See All</a>
                    </div>
                </div>

                <div class="not-paid-user">
                    @foreach($course->videos as $video)
                    <div class="box --small-course-play">
                        <a href="{{ route('players.courses.videos.show', [str_pad($course->id, 4, '0', STR_PAD_LEFT), str_pad($video->id, 4, '0', STR_PAD_LEFT)]) }}" class="d-flex align-items-center">
                            <figure>
                                <img src="/assets-web/images/play-btn.png" alt="">
                            </figure>

                            <div class="content">
                                <h6 class="title">{{ $video->title }}</h6>
                                <p class="desc">{{ $video->description }}</p>
                            </div>

                            <p class="time">{{ $video->length }}

                        <span class="overlay-box"></span>
                        <a href="#" class="overlay-text">Buy Now</a>
                    </div>
                    @endforeach
                </div> --}}
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection
