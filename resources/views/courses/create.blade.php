@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">Create Course</h2>
            <a href="{{ route('admin.courses.index') }}" class="btn btn-primary float-right">Back to Courses</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('admin.courses.store') }}" method="POST" enctype="multipart/form-data">

                @include('courses._form')

                <input type="submit" value="Save" class="btn btn-primary">
                <a href="{{ route('admin.courses.index') }}" class="btn btn-warning">Cancel</a>
            </form>
        </div>
    </div>
@endsection
