@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">Assign Coaches to Course</h2>
            <a href="{{ route('admin.courses.index') }}" class="btn btn-primary float-right">Back to Courses</a>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.courses.assign_coaches', [$course->id]) }}" method="POST">
                @csrf

                <h2>Course: {{ $course->title }}</h2>

                <div class="control-group">
                    <label for="coaches">Assign Coaches</label>
                    @foreach($coaches as $coach)
                        <div class="col-md-4">
                            <input type="checkbox" name="coaches[]"
                               @if(in_array($coach->id, $course_coaches)) checked @endif
                            value="{{ $coach->id }}"> {{ $coach->name }}
                        </div>
                    @endforeach
                </div>

                <input type="submit" value="Update" class="btn btn-primary">
                <a href="{{ route('admin.courses.index') }}" class="btn btn-warning">Cancel</a>
            </form>
        </div>
    </div>
@endsection
