@extends('layouts.app')

@section('content')
@include('partials._header')

    <section class="dashboard-layout bg-grey">
        {{--<header class="dashboard-header bg-red">
            <div class="container">
                <div class="header-content">
                    <h4 class="title">Hi, LaLiga Academy</h4>
                    <p class="desc">What course do you want to learn?</p>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item fc-black" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>

                </div>
            </div>
        </header> --}}

        <section class="dashboard-body">
            <div class="container">

                <h2 class="form-title text-center">Dashboard</h2>

                <div class="text-center mb-40">
                    <a href="{{ route('schedule', [session('session_id')]) }}" class="btn bg-red fc-white">
                        View Schedule
                    </a>
                </div>
                
                <div class="dashboard-course-list">
                    <div class="row align-items-center">
                        @foreach($active_courses as $course)
                        <div class="col-md-6">
                            <div class="box --course-btn-box">

                                <a href="{{ route('players.courses.show', [session('session_id'), str_pad($course->id, 4, '0', STR_PAD_LEFT)]) }}" class="btn --btn-outline text-center">
                                    {{ $course->category }} <br>
                                    {{ $course->title }}
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                {{-- <div class="course-slider">
                    @foreach($active_courses as $course)
                    <div class="course-item">
                        <a href="{{ route('players.courses.show', [str_pad($course->id, 4, '0', STR_PAD_LEFT)]) }}" class="box --course-box" style="background-image: url('{{ empty($course->image) ? asset("assets-web/images/course-img1.jpg") : Storage::url($course->image) }}');">
                            <div class="content">
                                <h6 class="title">{{ $course->title }}</h6>
                                <p class="desc">{{ $course->category }}</p>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div> 

                <div class="row align-items-center">
                    <div class="col-8">
                        <p class="maintitle">Upcoming Course</p>
                    </div>

                    <div class="col-4">
                        <a href="#" class="btn --btn-link float-right">See All</a>
                    </div>
                </div>


                <div class="row">
                    @foreach($coming_soon_courses as $course)
                    <div class="col-md-4">
                        <div class="box --small-course-box">
                            <div class="d-flex align-items-center">
                                <figure style="background-image: url('{{ empty($course->image) ? asset("assets-web/images/course-img1.jpg") : Storage::url($course->image) }}');"></figure>

                                <div class="content">
                                    <h6 class="title">{{ $course->title }}</h6>
                                    <p class="desc">{{ $course->category }}</p>
                                    <p class="date">coming soon</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div> --}}
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection
