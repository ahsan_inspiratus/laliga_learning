@extends('layouts.app')

@section('content')
@include('partials._header')

    <section class="dashboard-layout bg-grey">
        <section class="dashboard-body">
            <div class="container">
            	<header class="form-header">
                    <picture>
                        <img src="{{ asset("assets-web/images/laliga-academy-logo.png") }}" alt="" class="m-auto">
                    </picture>
                </header>

                <h2 class="form-title text-center">Weekly Schedule</h2>

                <div class="mobile-schedule d-block d-md-none">
                	<a href="{{ asset("assets-web/images/mobile-schedule.jpg") }}" data-fancybox>
                		<img src="{{ asset("assets-web/images/mobile-schedule.jpg") }}" alt="" class="m-auto">
                	</a>
                </div>

				<div class="d-none d-md-block">
	                <table class="table --schedule-table">
	                	<tr>
	                		<th>
	                			
	                		</th>

	                		<th>
	                			4:30 <br> 5:00 (GMT)
	                		</th>

	                		<th>
	                			5:00 <br> 5:30 (GMT)
	                		</th>

	                		<th>
	                			4:30 <br> 5:00 (GMT)
	                		</th>

	                		<th>
	                			5:00 <br> 5:30 (GMT)
	                		</th>
	                	</tr>

	                	<tr>
	                		<td>
	                			<span class="fc-yellow">Category</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">Development <br>U10-U12</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">Development <br>U14, U16 & U18</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">Adv-HPC <br>U9-U10 U12</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">Adv HPC <br>U14-U15-U16 & U18</span>
	                		</td>
	                	</tr>

	                	<tr>
	                		<td>
	                			<span class="fc-yellow">Sunday</span>
	                		</td>

	                		<td>
	                			<span class="fc-white"> - </span>
	                		</td>

	                		<td>
	                			<span class="fc-white"> - </span>
	                		</td>

	                		<td>
	                			<span class="fc-red">Training <br> Session</span>
	                		</td>

	                		<td>
	                			<span class="fc-red">Training <br> Session</span>
	                		</td>
	                	</tr>

	                	<tr>
	                		<td>
	                			<span class="fc-yellow">Monday</span>
	                		</td>

	                		<td>
	                			<span class="fc-red">Training <br> Session</span>
	                		</td>

	                		<td>
	                			<span class="fc-red">Training <br> Session</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">-</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">-</span>
	                		</td>
	                	</tr>
						
						<tr>
	                		<td>
	                			<span class="fc-yellow">Tuesday</span>
	                		</td>

	                		<td>
	                			<span class="fc-white"> - </span>
	                		</td>

	                		<td>
	                			<span class="fc-white"> - </span>
	                		</td>

	                		<td>
	                			<span class="fc-red">Training <br> Session</span>
	                		</td>

	                		<td>
	                			<span class="fc-red">Training <br> Session</span>
	                		</td>
	                	</tr>

	                	<tr>
	                		<td>
	                			<span class="fc-yellow">Wednesday</span>
	                		</td>

	                		<td>
	                			<span class="fc-red">Training <br> Session</span>
	                		</td>

	                		<td>
	                			<span class="fc-red">Training <br> Session</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">-</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">-</span>
	                		</td>
	                	</tr>

	                	<tr>
	                		<td>
	                			<span class="fc-yellow">Thursday</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">Activity, <br> Games, <br> Quiz</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">Theory, <br> Analysis, <br> Quiz</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">Activity, <br> Games, <br> Quiz</span>
	                		</td>

	                		<td>
	                			<span class="fc-white">Theory, <br> Analysis, <br> Quiz</span>
	                		</td>
	                	</tr>

	                </table>
	            </div>
                
                
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection
