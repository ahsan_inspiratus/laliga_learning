@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">Registered Players</h2>
            @if(Auth::user()->type == USER_TYPE_ADMIN)
            <a class="float-right" href="{{ route('admin.players.export_all') }}" target="_blank">
                <button class="btn btn-primary">Export All</button>
            </a>
            @endif
        </div>
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Gender</th>
                    <th>DOB</th>
                    <th>Country</th>
                    <th>Registered At</th>
                    <th>Email Verified</th>
                    <th>Already Paid</th>
                </tr>
                </thead>

                <tbody>
                @php
                    $ii = 1 + (($players->currentPage() - 1) * 20);
                @endphp
                @foreach($players as $player)
                    <tr>
                        <td>{{ $ii++ }}</td>
                        <td>{{ $player->name }}</td>
                        <td>{{ $player->email }}</td>
                        <td>{{ $player->phone }}</td>
                        <td>{{ $player->gender ?? '-' }}</td>
                        <td>@if(isset($player->dob))
                            {{ \Carbon\Carbon::parse($player->dob)->format('d/m/Y') }}
                            @else
                                -
                            @endif</td>
                        <td>{{ $player->country }}</td>
                        <td>{{ \Carbon\Carbon::parse($player->created_at)->format('d/m/Y h:ia') }}</td>
                        <td>
                            @if(!empty($player->email_verified_at))
                                <span class="badge badge-success">Yes</span>
                            @else
                                <span class="badge badge-info">No</span>
                            @endif
                        </td>
                        <td>
                            <input type="checkbox" value="{{ $player->id }}" class="already_paid_checkbox" @if($player->already_paid) checked="checked" @endif />
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $players->links() }}
            {{--<ul class="pagination">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
            </ul>--}}
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function(){
            $('.already_paid_checkbox').change(function(){
                var user_id = $(this).val();
                var is_checked = 0;
                if($(this). prop("checked") == true){
                    is_checked = 1;
                }
                $.post('/admin/players/'+user_id+'/updatePaymentStatus',
                    {already_paid: is_checked}
                ,function(result){
                    // alert(result);
                });
            });
        });
    </script>
@endsection
