@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.app')

@section('content')
@include('partials._header')

    <section class="dashboard-layout bg-grey">
        <section class="dashboard-body">
            <div class="container">
                <section class="form-container">

                    <h2 class="form-title">{{ empty($user->dob) ? 'Complete' : 'Update' }} Profile</h2>

                    <form action="{{ route('players.profile_update', [session('session_id')]) }}" method="POST" class="default-form">
                        @csrf
                        <div class="control-group">
                            <label class="form-label">
                                Name:
                            </label>

                            <input id="name" type="text" class="form-field bg-spgrey @error('name') is-invalid @enderror"  name="name" value="{{ old('name') ?? $user->name }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="control-group">
                            <label class="form-label">
                                Email:
                            </label>

                            <input name="email" id="email" value="{{ $user->email }}" type="email" disabled class="form-field bg-spgrey">
                        </div>

                        <div class="control-group">
                            <label class="form-label">
                                Gender:
                            </label>

                            <select name="gender" class="form-field bg-spgrey" required>
                                <option value="">Select Gender</option>
                                <option value="Male" @if(old('gender') ?? $user->gender == 'Male') selected="selected" @endif>Male</option>
                                <option value="Female" @if(old('gender') ?? $user->gender == 'Female') selected="selected" @endif>Female</option>
                            </select>
                        </div>

                        <div class="control-group">
                            <label class="form-label">
                                Phone:
                            </label>

                            <input name="phone" type="number" class="form-field bg-spgrey @error('phone') is-invalid @enderror" value="{{ old('phone') ?? $user->phone }}" required>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="control-group">
                            <label class="form-label">
                                Date of Birth:
                            </label>

                            <input id="dob" type="date" name="dob" placeholder="mm/dd/yyyy" class="form-field bg-spgrey" value="{{ old('dob') ?? $user->dob }}" required>
                        </div>

                        <div class="control-group">
                            <label class="form-label">
                                Current Football Academy:
                            </label>

                            <input id="current_football_academy" name="current_football_academy" type="text" class="form-field bg-spgrey" value="{{ old('current_football_academy') ?? $user->current_football_academy }}">
                        </div>

                        <div class="control-group">
                            <label class="form-label">
                                Location:
                            </label>

                            <input id="location" name="location" type="text" class="form-field bg-spgrey" value="{{ old('location') ?? $user->location }}">
                        </div>

                        <div class="control-group">
                            <label class="form-label">
                                Country:
                            </label>

                            <select name="country" class="form-field bg-spgrey" required>
                                <option value="">Select Country</option>
                                @foreach($arrays::countries() as $country)
                                    <option value="{{ $country }}" @if(old('country') ?? $user->country == $country) selected="selected" @endif>{{ $country }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="control-group">
                            <input type="submit" value="Update Profile" class="btn --btn-submit">
                        </div>

                    </form>
                </section>
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection
