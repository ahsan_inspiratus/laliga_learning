<section class="mobile-navigation">
    <header class="primary-header">
        <p class="title">Menu</p>
    </header>
    <nav class="navigation-container">
        <ul class="unstyled">
            <li>
                <a href="{{ route('dashboard', session('session_id') ?? '###') }}">
                    <i class="xicon icon-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="{{ route('players.profile', session('session_id') ?? '###') }}">
                    <i class="xicon icon-user"></i> Profile
                </a>
            </li>
            <li>
                <a href="{{ route('schedule', session('session_id') ?? '###') }}">
                    <i class="xicon icon-calendar"></i> Schedule
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="xicon icon-dashboard"></i> About
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="xicon icon-logout"></i> Log Out
                </a>
            </li>
        </ul>
    </nav>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>

    <footer class="primary-footer">
        <p class="desc fc-default fs-small">Copyright &copy; LaLiga Academy UAE.</p>
    </footer>
</section>
