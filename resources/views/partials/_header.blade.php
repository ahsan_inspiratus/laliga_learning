<header class="primary-header">
    <div class="d-flex align-items-center no-gutters">
        <div class="col-3"></div>

        <div class="col-6">
            <a href="/" class="header-logo">
                <img src="/assets-web/images/header-logo.png" alt="">
            </a>
        </div>

        <div class="col-3">
            <div class="float-right">
                <!-- Mobile Navigation Button Start-->
                <div class="mobile-nav-btn">
                    <span class="lines"></span>
                    <span class="lines"></span>
                    <span class="lines"></span>
                    <span class="lines"></span>
                </div>
                <!-- Mobile Navigation Button End-->
            </div>
        </div>
    </div>
</header>