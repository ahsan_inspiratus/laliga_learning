<footer class="primary-footer">
    <ul class="unstyled inline social-media-footer">
        <li>
            <a href="https://www.facebook.com/laligaacademyuae/" target="_blank">
                <i class="xicon icon-facebook facebook"></i>
            </a>
        </li>

        <li>
            <a href="https://twitter.com/laligacademyuae/" target="_blank">
                <i class="xicon icon-twitter twitter"></i>
            </a>
        </li>

        <li>
            <a href="https://www.youtube.com/channel/UCpVY9k5KPP4kvrG4uF66TNg" target="_blank">
                <i class="xicon icon-youtube youtube"></i>
            </a>
        </li>

        <li>
            <a href="https://www.instagram.com/laligaacademyuae/" target="_blank">
                <i class="xicon icon-instagram instagram"></i>
            </a>
        </li>
    </ul>
</footer>