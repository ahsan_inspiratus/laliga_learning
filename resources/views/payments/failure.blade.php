@extends('layouts.app')

@section('content')
    @include('partials._header')
    <section class="dashboard-layout bg-grey">

        <section class="dashboard-body">
            <div class="container">
                <h2 class="form-title text-center">
                    @if(isset($course->title)) {{ $course->title.' - ' }} @endif
                        Payment Failed</h2>
                <p>
                    @if($course)
                    <a href="{{ route('players.courses.show', [$session_id, $course]) }}">Click here to go back to Payment Page</a>
                        @else
                        <a href="{{ route('dashboard', [$session_id]) }}">Click here to go back to Courses Page.</a>
                    @endif
                </p>
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
        });
    </script>
@endsection
