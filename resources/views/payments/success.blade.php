@extends('layouts.app')

@section('content')
    @include('partials._header')
    <section class="dashboard-layout bg-grey">

        <section class="dashboard-body">
            <div class="container">
                <h2 class="form-title text-center">{{ $course->title }} - Payment Successful</h2>
                <p>
                    <a href="{{ route('players.courses.show', [$session_id, $course]) }}">Click here to go back to Course Page</a>
                </p>
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
        });
    </script>
@endsection
