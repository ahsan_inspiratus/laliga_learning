@extends('layouts.app')

@section('content')
    @include('partials._header')
    <section class="dashboard-layout bg-grey">

        <section class="dashboard-body">
            <div class="container">
                <h2 class="form-title text-center">{{ $course->title }} - Payment Processing</h2>
                <form action="https://ae.laligacademy.com/payment/laligacademyOnlineCheckout" method="POST" id="paymentForm">
                    <input type="hidden" name="payment_id" value="{{ $payment_id }}" />
                    <input type="hidden" name="user_email" value="{{ Auth::user()->email }}" />
                    <input type="hidden" name="user_name" value="{{ Auth::user()->name }}" />
                    <input type="hidden" name="total_amount" value="{{ $total_amount }}" />
                    <input type="hidden" name="item_name" value="{{ $item_name }}" />
                </form>
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection

@section('footer-scripts')
    {{--<script type="text/javascript" src="{{ asset('assets-web/js/checkout.js') }}"></script>
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}'
                }
            });
            getPaymentPage('creditcard');
        });
    </script>--}}
    <script>
        $(window).load(function(){
            $('#paymentForm').submit();
        });
    </script>
@endsection
