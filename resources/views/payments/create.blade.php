@extends('layouts.app')

@section('content')
    @include('partials._header')

    <section class="dashboard-layout bg-grey">

        <section class="dashboard-body">
            <div class="container">
                <h2 class="form-title text-center">{{ $course->title }} - Payment</h2>
                <div class="row">

                    <div class="col-md-4 offset-md-4">
                        <div class="box --payment-box">
                            <h4 class="title">1 Week Payment</h4>
                            <p class="price">
                                <strong>210.00 AED</strong>
                            </p>
                            <p class="date">
                                <strong>{{ $dates['coming_sunday']->format('d/m/Y') }}</strong>
                                to
                                <strong>{{ $dates['day_after_1_week']->format('d/m/Y') }}</strong>
                            </p>

                            <form action="{{ route('players.payments.checkout', [session('session_id'), $course, 3]) }}" method="post">
                                @csrf
                                <button type="submit" class="btn --btn-primary">Pay Now</button>
                            </form>
                        </div>
                    </div>

                    {{--<div class="col-md-4">
                        <div class="box --payment-box">
                            <h4 class="title">4 Weeks Payment</h4>
                            <p class="price">
                                <strong>830.00 AED</strong>
                                <strong>320.00 CAD</strong>
                            </p>
                            <p class="date">
                                <strong>{{ $dates['coming_sunday']->format('d/m/Y') }}</strong>
                                to
                                <strong>{{ $dates['day_after_4_weeks']->format('d/m/Y') }}</strong>
                            </p>

                            <form action="{{ route('players.payments.checkout', [$course, 1]) }}" method="post">
                                @csrf
                                <button type="submit" class="btn --btn-primary">Pay Now</button>
                            </form>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box --payment-box">
                            <h4 class="title">8 Weeks Payment</h4>
                            <p class="price">
                                <strong>1425.00 AED</strong>
                                <strong>550.00 CAD</strong>
                            </p>
                            <p class="date">
                                <strong>{{ $dates['coming_sunday']->format('d/m/Y') }}</strong>
                                to
                                <strong>{{ $dates['day_after_8_weeks']->format('d/m/Y') }}</strong>
                            </p>

                            <form action="{{ route('players.payments.checkout', [$course, 2]) }}" method="post">
                                @csrf
                                <button type="submit" class="btn --btn-primary">Pay Now</button>
                            </form></button>
                        </div>
                    </div>--}}

                </div>
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
        });
    </script>
@endsection
