@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">{{ $course->title }} - {{ $questionnaire->title }} - Quizzes</h2>
            <a href="{{ route('coach.courses.questionnaires.quizzes.create', [$course->id, $questionnaire->id]) }}" class="btn btn-primary float-right">+ Add Quiz</a>
        </div>
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>Question</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @php
                    $ii = 1;
                @endphp
                @foreach($questionnaire->quizzes as $quiz)
                    <tr>
                        <td>{{ $ii++ }}</td>
                        <td>{{ $quiz->question }}</td>
                        <td>{{ $quiz->type == QUIZ_TYPE_MCQS ? 'MCQs' : 'True/False'}}</td>
                        <td>
                            <a class="btn btn-info btn-sm" href="{{ route('coach.courses.questionnaires.quizzes.edit', [$course->id, $questionnaire->id, $quiz->id]) }}">Edit</a>
                            <a class="btn --btn-primary btn-sm deleteItem" data-delete-id="{{ $quiz->id }}" href="#">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{--<ul class="pagination">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
            </ul>--}}
        </div>
    </div>
    <form action="" id="deleteForm" method="POST" >
        {{ method_field('DELETE')}}
        @csrf
    </form>
@endsection

@section('footer-scripts')
    <script>
        var deleteResourceUrl = '{{ url()->current() }}';
        $(document).ready(function () {

            $('.deleteItem').click(function (e) {
                e.preventDefault();
                var deleteId = parseInt($(this).data('deleteId'));

                $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);

                if (confirm('Are you sure?')) {
                    $('#deleteForm').submit();
                }
            });

        });
    </script>
@endsection
