@csrf
<div class="control-group">
    <label for="question">Question</label>
    <input type="text" name="question" value="{{ old('question', $quiz->question ?? '') }}" required class="form-control">
</div>

<div class="control-group">
    <label for="type">Type</label>
    <input type="radio" name="type" value="{{ QUIZ_TYPE_MCQS }}" checked> MCQ
    <input type="radio" name="type" value="{{ QUIZ_TYPE_TRUE_FALSE }}" @if(old('type', $quiz->type ?? '') == QUIZ_TYPE_TRUE_FALSE) checked @endif> True/False
    {{--<input type="radio" name="type" value="{{ QUIZ_TYPE_SHORT_QA }}" @if(old('type', $quiz->type ?? '') == QUIZ_TYPE_SHORT_QA) checked @endif> Short Q/A--}}
</div>


<table id="mcqs_options" class="options_div" style="display: none;">
    <tr>
        <th>Answer</th>
        <th>Options</th>
    </tr>
    <tr>
        <td><input type="radio" name="answer" value="1" checked></td>
        <td><input type="text" name="options[]" class="form-control" @if(isset($quiz))
            value="{{ json_decode($quiz->options)[0] ?? '' }}" @endif></td>
    </tr>
    <tr>
        <td><input type="radio" name="answer" value="2"></td>
        <td><input type="text" name="options[]" class="form-control" @if(isset($quiz))
            value="{{ json_decode($quiz->options)[1] ?? '' }}" @endif></td>
    </tr>
    <tr>
        <td><input type="radio" name="answer" value="3"></td>
        <td><input type="text" name="options[]" class="form-control" @if(isset($quiz))
            value="{{ json_decode($quiz->options)[2] ?? '' }}" @endif></td>
    </tr>
    <tr>
        <td><input type="radio" name="answer" value="4"></td>
        <td><input type="text" name="options[]" class="form-control" @if(isset($quiz))
            value="{{ json_decode($quiz->options)[3] ?? '' }}" @endif></td>
    </tr>
</table>

<table id="true_false_options" class="options_div" style="display: none;">
    <tr>
        <th>Answer</th>
        <th>Options</th>
    </tr>
    <tr>
        <td><input type="radio" name="answer" value="1" checked></td>
        <td>True</td>
    </tr>
    <tr>
        <td><input type="radio" name="answer" value="0"></td>
        <td>False</td>
    </tr>
</table>

<div class="control-group">
    <label for="media_type">Attached Media Type</label>
    <input type="radio" name="media_type" value="0" checked> None
    <input type="radio" name="media_type" value="{{ MEDIA_TYPE_IMAGE }}" @if(old('media_type', $quiz->media_type ?? '') == MEDIA_TYPE_IMAGE) checked @endif> Image
    <input type="radio" name="media_type" value="{{ MEDIA_TYPE_VIDEO }}" @if(old('media_type', $quiz->media_type ?? '') == MEDIA_TYPE_VIDEO) checked @endif> Video
    <br><br>
    @if(isset($quiz->media_type) && $quiz->media_type != 0 && $quiz->media)
        <strong>Current Media</strong><br>
        @if($quiz->media_type == MEDIA_TYPE_IMAGE)
            <img src="{{ $quiz->media }}" alt="*" style="max-width: 200px; max-height: 150px">
        @elseif($quiz->media_type == MEDIA_TYPE_VIDEO)
            <video controls width="80%">
                <source src="{{ $quiz->media }}" type="video/mp4">
                Sorry, your browser doesn't support embedded videos.
            </video>
        @endif
    <br><br>
    @endif
    <input type="file" name="media" accept="image/*" id="media_file_input">
</div>

<div class="control-group">
    <label for="active_descriptive_field">Descriptive Field</label>
    <input type="radio" name="active_descriptive_field" required value="0" @if(old('active_descriptive_field', $quiz->active_descriptive_field ?? '') == 0) checked @endif> Disabled
    <input type="radio" name="active_descriptive_field" required value="1" @if(old('active_descriptive_field', $quiz->active_descriptive_field ?? '') == 1) checked @endif> Enabled
</div>
