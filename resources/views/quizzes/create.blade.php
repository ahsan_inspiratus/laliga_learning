@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">Create Quiz</h2>
            <a href="{{ route('coach.courses.questionnaires.quizzes.index', [$course->id, $questionnaire->id]) }}" class="btn btn-primary float-right">Back to Quizzes</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('coach.courses.questionnaires.quizzes.store', [$course->id, $questionnaire->id]) }}" method="POST" enctype="multipart/form-data">

                @include('quizzes._form')

                <input type="submit" value="Save" class="btn btn-primary">
                <a href="{{ route('coach.courses.questionnaires.quizzes.index', [$course->id, $questionnaire->id]) }}" class="btn btn-warning">Cancel</a>
            </form>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
            $("input[name='type']").change(function(){
                hideShowOptionsDiv();
            });
            function hideShowOptionsDiv(){
                $('.options_div').hide();
                var type = $("input[name='type']:checked").val();
                if(type == '{{ QUIZ_TYPE_MCQS }}'){
                    $('#mcqs_options').show();
                    $('#mcqs_options input:radio[name=answer]')[0].checked = true;
                } else if(type == '{{ QUIZ_TYPE_TRUE_FALSE }}') {
                    $('#true_false_options').show();
                    $('#true_false_options input:radio[name=answer]')[0].checked = true;
                } /*else {
                    $('#').hide();
                }*/
            }
            hideShowOptionsDiv();

            $("input[name='media_type']").change(function(){
                hideShowMediaFileInput();
            });
            function hideShowMediaFileInput(){
                var media_type = $("input[name='media_type']:checked").val();
                if(media_type == '{{ MEDIA_TYPE_IMAGE }}'){
                    $('#media_file_input').show();
                    $('#media_file_input').attr('accept', 'image/*');
                } else if(media_type == '{{ MEDIA_TYPE_VIDEO }}'){
                    $('#media_file_input').show();
                    $('#media_file_input').attr('accept', 'video/*');
                } else {
                    $('#media_file_input').hide();
                }
            }
            hideShowMediaFileInput();
        });
    </script>
@endsection
