<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title><?php echo isset($title) ? $title.' | ' : ''  ?>{{ config('app.name', 'Laravel') }}</title>

    <meta name="keywords" content="<?php echo $keywords ?? '' ?>" />
    <meta name="description" content="<?php echo $desc ?? '' ?>" />
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <noscript id="deferred-styles">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900|Oswald:400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset("/assets-web/css/style.css") }}"/>
    </noscript>

    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = requestAnimationFrame || mozRequestAnimationFrame ||
            webkitRequestAnimationFrame || msRequestAnimationFrame;
        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        else window.addEventListener('load', loadDeferredStyles);
    </script>

    <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .pre-loading {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('{{ asset("/assets-web/images/loader.gif") }}') center no-repeat #24282b;
        }
        .pre-loading strong{
            position: fixed;
            left: 40%;
            top: 70%;
        }
    </style>
</head>
<body class="<?php echo $pageclass ?? '' ?>">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="pre-loading">
    <strong class="fc-red" style="display:none;">Video is uploading, please don't refresh.</strong>
</div>

<main class="app-container" id="app">

    @include('partials._navigation')

    {{--
    <header class="primary-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-4 col-xs-8">
                    <a href="{{ route('home') }}" class="header-logo">
                        <img src="{{ asset("/assets-web/images/header-logo.png") }}" alt="">
                    </a>
                </div>
                <div class="col-lg-9 col-md-8 col-xs-4">
                    <nav class="d-none d-sm-block">
                        <ul class="unstyled inline primary-navigation">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </nav>

                    <!-- Mobile Navigation Button Start-->
                    <div class="mobile-nav-btn d-block d-md-none">
                        <span class="lines"></span>
                        <span class="lines"></span>
                        <span class="lines"></span>
                        <span class="lines"></span>
                    </div>
                    <!-- Mobile Navigation Button End-->
                </div>
            </div>
        </div>
    </header>

    <!-- Mobile Navigation START -->
    <nav class="mobile-navigation d-block d-sm-none">
        <ul class="unstyled">

        </ul>
    </nav>
    <!-- Mobile Navigation END -->

    --}}


    @yield('content')

</main>

<!-- <div class="overlay-bg"></div> -->

<script type="text/javascript" src="{{ asset("assets-web/js/functions.js") }}"></script>
<script type="text/javascript" src="{{ asset("assets-web/js/script.js") }}"></script>

<script>
    $(window).load(function() {
        // Animate loader off screen
        $(".pre-loading").fadeOut("slow");
    });
</script>
@yield('footer-scripts')
</body>
</html>
