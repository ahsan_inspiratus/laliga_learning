<?php
    $user = Auth::user()->type == USER_TYPE_ADMIN ? 'admin.' : 'coach.';
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title><?php echo isset($title) ? $title.' | ' : ''  ?>Dashboard</title>
    <meta name="keywords" content="<?php echo $keywords ?? '' ?>" />
    <meta name="description" content="<?php echo $desc ?? '' ?>" />
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <noscript id="deferred-styles">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets-backend/css/style.css') }}"/>
    </noscript>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = requestAnimationFrame || mozRequestAnimationFrame ||
            webkitRequestAnimationFrame || msRequestAnimationFrame;
        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        else window.addEventListener('load', loadDeferredStyles);
    </script>

    <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .pre-loading {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('{{ asset("/assets-backend/images/loader.gif") }}') center no-repeat #fff;
        }
    </style>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show pace-done">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="pre-loading"></div>


<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{{ asset('assets-backend/images/inspiratus-logo.png') }}" width="160" height="40" alt="Inspiratus">
        <img class="navbar-brand-minimized" src="{{ asset('assets-backend/images/inspiratus-logo.png') }}" width="30" height="30" alt="Inspiratus">
    </a>

    <ul class="nav navbar-nav ml-auto mr-4">

        <li class="nav-item dropdown">
            <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <!-- <img class="img-avatar" src="{{ asset('assets-backend/images/avatar.jpg') }}" alt="admin@inspirat.us"> -->
                {{ Auth::user()->name }} ({{ Auth::user()->email }})
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">

    <div class="sidebar">
        <nav class="sidebar-nav ps ps--active-y">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route($user.'dashboard') }}">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                    </a>
                </li>

                <li class="nav-title">Menu</li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route($user.'courses.index') }}">
                        <i class="nav-icon icon-drop"></i> Courses
                    </a>
                </li>

                @if(Auth::user()->type == USER_TYPE_ADMIN)
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.coaches.index') }}">
                        <i class="nav-icon icon-user"></i> Coaches
                    </a>
                </li>
                @endif

                <li class="nav-item">
                    <a class="nav-link" href="{{ route($user.'players.index') }}">
                        <i class="nav-icon icon-user"></i> Registered Users
                    </a>
                </li>
            </ul>
        </nav>
    </div>

    <main class="main">

        <div class="container-fluid" style="margin-top: 40px">

            @yield('content')

        </div>

    </main>

</div>

<footer class="app-footer">
    <div>
        <span>Copyright &copy; <span id="cur-year"></span> Inspiratus Consultant LTD. All Rights Reserved. </span>
    </div>
</footer>

<!-- <div class="overlay-bg"></div> -->

<script type="text/javascript" src="{{ asset('assets-backend/js/functions.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets-backend/js/script.js') }}"></script>

<script>
    $(window).load(function() {
        // Animate loader off screen
        $(".pre-loading").fadeOut("slow");
    });
</script>
@yield('footer-scripts')
</body>
</html>
