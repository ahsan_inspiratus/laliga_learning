@extends('layouts.app')

@section('content')
    <section class="registration-layout --fixed align-items-center bg-grey">
        <div class="container">
            <section class="form-container">
                <header class="form-header">
                    <picture>
                        <img src="{{ asset("assets-web/images/laliga-academy-logo.png") }}" alt="" class="m-auto">
                    </picture>
                </header>

                <h2 class="form-title">{{ __('Reset Password') }}</h2>

                <form class="default-form" method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="control-group">

                        <input id="email" type="email" class="form-field bg-spgrey @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your Email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">
                        <input type="submit" value="{{ __('Reset Password') }}" class="btn --btn-submit">
                    </div>

                    @if (session('status'))
                        <div class="alert-success fc-red" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                </form>

                <footer class="primary-footer">
                    <p class="desc fc-default">Don't have an account? <a href="{{ route('register') }}" class="fc-yellow fw-semi-bold">Register</a> or <a href="{{ route('login') }}" class="fc-yellow fw-semi-bold">Login</a></p>
                </footer>
            </section>
        </div>
    </section>
@endsection
