@extends('layouts.app')

@section('content')
    <section class="registration-layout --fixed align-items-center bg-grey">
        <div class="container">
            <section class="form-container">
                <header class="form-header">
                    <picture>
                        <img src="{{ asset("assets-web/images/laliga-academy-logo.png") }}" alt="" class="m-auto">
                    </picture>
                </header>

                <h2 class="form-title">{{ __('Reset Password') }}</h2>

                <form class="default-form" method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="control-group">

                        <input id="email" type="email" class="form-field bg-spgrey @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Enter your Email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">
                        <label class="form-label">
                            Password:
                        </label>

                        <input id="password" type="password" class="form-field bg-spgrey @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">
                        <label class="form-label">
                            Confirm Password:
                        </label>

                        <input id="password-confirm" type="password" class="form-field bg-spgrey" name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <div class="control-group">
                        <input type="submit" value="{{ __('Reset Password') }}" class="btn --btn-primary w-100">
                    </div>

                </form>

                <footer class="primary-footer">
                    <p class="desc fc-default">Already have an account? <a href="{{ route('login') }}" class="fc-red fw-semi-bold">Login</a></p>
                </footer>
            </section>
        </div>
    </section>
@endsection
