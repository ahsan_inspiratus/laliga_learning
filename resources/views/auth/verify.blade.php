@extends('layouts.app')

@section('content')
    <section class="registration-layout --fixed align-items-center bg-grey">
        <div class="container">
            <section class="form-container">
                <header class="form-header">
                    <picture>
                        <img src="{{ asset("assets-web/images/laliga-academy-logo.png") }}" alt="" class="m-auto">
                    </picture>
                </header>

                @if (session('resent'))
                    <div class="alert --alert-success" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>
                @endif

                <h2 class="form-title">{{ __('Verification') }}</h2>

                <p class="desc fc-default mb-20">{{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email.') }}
                </p>

                <form class="default-form" method="POST" action="{{ route('verification.resend') }}">
                    @csrf
                    <button type="submit" class="btn --btn-submit">{{ __('Click to Request Another') }}</button>.
                </form>

                <footer class="primary-footer">
                    <p class="desc fc-default">Already have an account? <a href="{{ route('login') }}" class="fc-yellow fw-semi-bold">Login</a></p>
                </footer>
            </section>
        </div>
    </section>
@endsection
