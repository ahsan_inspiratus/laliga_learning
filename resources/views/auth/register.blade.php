@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.app')

@section('content')
    <section class="registration-layout bg-grey">
        <div class="container">
            <section class="form-container">
                <header class="form-header">
                    <picture>
                        <img src="{{ asset("assets-web/images/laliga-academy-logo.png") }}" alt="" class="m-auto">
                    </picture>
                </header>

                <h2 class="form-title">{{ __('Registration') }}</h2>

                <form class="default-form" method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="control-group">

                        <input id="name" type="text" class="form-field bg-spgrey @error('name') is-invalid @enderror"  name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter your Name">
                        @error('name')
                        <span class="error-field" role="alert">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">

                        <input id="email" type="email" class="form-field bg-spgrey @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter your Email">
                        @error('email')
                        <span class="error-field" role="alert">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">

                        <input name="phone" type="number" class="form-field bg-spgrey @error('phone') is-invalid @enderror" value="{{ old('phone') }}" pattern=".{5,10}" required placeholder="Enter your Number">
                        @error('phone')
                        <span class="error-field" role="alert">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">

                        <select name="country" class="form-field bg-spgrey" required>
                            <option value="">Select Country</option>
                            @foreach($arrays::countries() as $country)
                                <option value="{{ $country }}" @if(old('country') == $country) selected="selected" @endif>{{ $country }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="control-group">

                        <input id="password" type="password" class="form-field bg-spgrey @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Create Password">

                        @error('password')
                        <span class="error-field" role="alert">
                            {{ $message }}
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">

                        <input id="password-confirm" type="password" class="form-field bg-spgrey" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                    </div>

                    <div class="control-group">
                        <div class="checkbox">
                            <input type="checkbox" required>
                            <label class="label"></label>
                            <p class="text">
                                I read and accept the <a href="/terms-conditions" class="fc-yellow fw-semi-bold">Terms and Conditions</a>
                            </p>
                        </div>
                    </div>

                    <div class="control-group">
                        <input type="submit" value="{{ __('Register') }}" class="btn --btn-submit">
                    </div>

                </form>
            </section>
        </div>

        <footer class="primary-footer">
            <p class="desc fc-default">Already have an account? <a href="{{ route('login') }}" class="fc-yellow fw-semi-bold">Login</a></p>
        </footer>

    </section>
@endsection
