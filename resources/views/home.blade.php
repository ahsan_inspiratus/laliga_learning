@extends('layouts.app')

@section('content')
    <section class="registration-layout --main-screen bg-grey">
        <div class="container">
            <section class="form-container">
                <header class="form-header">
                    <picture>
                        <img src="{{ asset("assets-web/images/laliga-academy-logo.png") }}" alt="" class="m-auto">
                    </picture>
                </header>

                <article class="main-text mt-40 mb-40 text-center">
                    <p class="fs-medium lh-large">Designed to LaLiga’s high standards and developed by our UEFA-Pro Certified LaLiga coaches, this is a comprehensive, multi-faceted program offering the best of both theory and practice to your children, with live virtual football skills lessons, recorded fitness training videos, theory lessons to enhance players’ strategy and tactics, Game IQ, match analysis sessions and much more!</p>
                </article>

                <div class="text-center mb-40">
                    <a href="{{ route('schedule') }}" class="btn bg-red fc-white">
                        View Schedule
                    </a>
                </div>

                <div class="homepage-image">
                    <figure>
                        <img src="{{ asset("assets-web/images/homepage-laliga-image.png") }}" alt="" class="m-auto">
                    </figure>
                </div>

                <form action="" class="default-form --registration-form pb-20">

                    <div class="row no-gutters">                        

                        <div class="col-6">
                            <div class="control-group">
                                <a href="{{ route('register') }}" class="btn --btn-registration">Register</a>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="control-group">
                                <a href="{{ route('login') }}" class="btn --btn-login">Login</a>
                            </div>
                        </div>

                    </div>

                </form>
            </section>
        </div>
    </section>
    @include('partials._socialfooter')
@endsection
