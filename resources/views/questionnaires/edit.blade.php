@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">Edit Questionnaire</h2>
            <a href="{{ route('coach.courses.questionnaires.index', [$course->id]) }}" class="btn btn-primary float-right">Back to Questionnaires</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('coach.courses.questionnaires.update', [$course->id, $questionnaire->id]) }}" method="POST">
                {{ method_field('PATCH')}}
                @include('questionnaires._form')

                <input type="submit" value="Save" class="btn btn-primary">
                <a href="{{ route('coach.courses.questionnaires.index', [$course->id]) }}" class="btn btn-warning">Cancel</a>
            </form>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
        });
    </script>
@endsection
