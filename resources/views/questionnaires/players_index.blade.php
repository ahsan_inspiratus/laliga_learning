@extends('layouts.app')

@section('content')
    @include('partials._header')

    <section class="dashboard-layout bg-grey">

        <section class="dashboard-body">
            <div class="container">
                <h2 class="form-title">{{ $course->title }} - Questionnaires</h2>
                @forelse($course->questionnaires as $questionnaire)
                <div class="box --question-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h4 class="title">{{ $questionnaire->title }}</h4>
                            <p class="desc">{{ $questionnaire->description }}</p>
                        </div>

                        <div class="col-md-4">
                            <div class="text-right">
                                <a href="{{ route('players.courses.questionnaires.show', [session('session_id'), $course->id, $questionnaire->id]) }}" class="btn --btn-primary">Take the Quiz</a>
                            </div>
                        </div>
                        </div>
                    </div>
                @empty
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p>No Questionnaires.</p>
                        </div>
                    </div>
                @endforelse
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
        });
    </script>
@endsection
