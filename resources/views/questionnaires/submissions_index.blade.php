@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">{{ $course->title }} - {{ $questionnaire->title }} - Submissions</h2>
        </div>
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>Submitted By</th>
                    <th>Score</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @php
                    $ii = 1;
                @endphp
                @foreach($submissions as $submission)
                    <tr>
                        <td>{{ $ii++ }}</td>
                        <td>{{ $submission->user->name }}</td>
                        <td>{{ $submission->getScore() }}/{{ $total_quizzes }}</td>
                        <td>{{ $submission->created_at }}</td>
                        <td>
                            <a class="btn btn-info btn-sm" target="_blank" href="{{ route('coach.courses.questionnaires.submissions.show', [$course, $questionnaire, $submission]) }}">View</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{--<ul class="pagination">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
            </ul>--}}
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function () {
        });
    </script>
@endsection
