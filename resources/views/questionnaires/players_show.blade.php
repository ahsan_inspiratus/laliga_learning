@extends('layouts.app')

@section('content')
    @include('partials._header')

    <section class="dashboard-layout bg-grey">

        <section class="dashboard-body">
            <div class="container">
                <h2 class="form-title">{{ $questionnaire->title }}</h2>
                @if($submission)
                <h4 class="mb-40">You Scored <span class="fc-yellow">{{ $submission->getScore() }}</span> out of <span class="fc-red">{{ $questionnaire->quizzes->count() }}</span></h4>
                @endif
                <form id="quiz_form" action="{{ route('players.courses.questionnaires.store', [session('session_id'), $questionnaire->course_id, $questionnaire->id]) }}" method="POST">
                    @csrf
                    @foreach($questionnaire->quizzes as $quiz)
                    <div class="box --quiz-box">
                        @if($quiz->media_type != 0 && $quiz->media)
                            @if($quiz->media_type == MEDIA_TYPE_IMAGE)
                                <img src="{{ $quiz->media }}" alt="*" style="max-width: 200px; max-height: 150px">
                            @elseif($quiz->media_type == MEDIA_TYPE_VIDEO)
                                <video controls width="80%">
                                    <source src="{{ $quiz->media }}" type="video/mp4">
                                    Sorry, your browser doesn't support embedded videos.
                                </video>
                            @endif
                            <br><br>
                        @endif
                        <p class="title">{{ $quiz->question }}</p>
                        @if($quiz->type == QUIZ_TYPE_MCQS)
                            @php
                                $ii = 0;
                            @endphp
                            @foreach(json_decode($quiz->options) as $option)
                                @if(!empty($option))
                                <div class="option-box">
                                    <input type="radio" class="quiz_radio" name="quiz_id_{{ $quiz->id }}" value="{{ ++$ii }}" @if($submission && $quiz->answer == $ii) checked @endif>
                                    <span class="label {{ $ii }}
                                    @if($submission && isset($answers['quiz_id_'.$quiz->id]) && $answers['quiz_id_'.$quiz->id] == $ii && $quiz->answer != $answers['quiz_id_'.$quiz->id])
                                        bg-red
                                    @endif
                                    ">{{ $option }}</span>
                                </div>
                                @endif
                            @endforeach
                        @else
                            <div class="option-box">
                                <input type="radio" class="quiz_radio" name="quiz_id_{{ $quiz->id }}" value="1" @if($submission && $quiz->answer == 1) checked @endif>
                                <span class="label
                                @if($submission && isset($answers['quiz_id_'.$quiz->id]) && $answers['quiz_id_'.$quiz->id] == 1 && $quiz->answer != $answers['quiz_id_'.$quiz->id])
                                    bg-red
                                @endif
                                ">True</span>
                            </div>
                            <div class="option-box">
                                <input type="radio" class="quiz_radio" name="quiz_id_{{ $quiz->id }}" value="0" @if($submission && $quiz->answer == 0) checked @endif>
                                <span class="label
                                @if($submission && isset($answers['quiz_id_'.$quiz->id]) && $answers['quiz_id_'.$quiz->id] == 0 && $quiz->answer != $answers['quiz_id_'.$quiz->id])
                                    bg-red
                                @endif
                                ">False</span>
                            </div>
                        @endif
                        @if($quiz->active_descriptive_field)
                                <textarea name="descriptive_field_{{ $quiz->id }}" id="descriptive_field_{{ $quiz->id }}" cols="30" rows="10">{{ $answers['descriptive_field_'.$quiz->id] ?? '' }}</textarea>
                        @endif
                    </div>
                    @endforeach
                    @if(!$submission)
                    <input type="button" class="btn --btn-submit" value="Submit">
                    @endif
                </form>
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
            $('.--btn-submit').click(function(){
                if($('input[class=quiz_radio]:checked').length == $('.box.--quiz-box p.title').length){
                    // Confirmation Box
                    if (confirm('Are you sure to submit the quiz?')) {
                        $('#quiz_form').submit();
                    }
                } else {
                    alert('Kindly select answers for all questions.');
                }
            });

            @if($submission)
            $('.box.--quiz-box input[type=radio]').attr('disabled','disabled');
            @endif
        });
    </script>
@endsection
