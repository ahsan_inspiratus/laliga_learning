@csrf
<div class="control-group">
    <label for="title">Title</label>
    <input type="text" name="title" value="{{ old('title', $questionnaire->title ?? '') }}" required class="form-control">
</div>

<div class="control-group">
    <label for="description">Description</label>
    <textarea name="description" class="form-control">{{ old('description', $questionnaire->description ?? '') }}</textarea>
</div>
