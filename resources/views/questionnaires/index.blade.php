@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">{{ $course->title }} - Questionnaires</h2>
            <a href="{{ route('coach.courses.questionnaires.create', [$course->id]) }}" class="btn btn-primary float-right">+ Add Questionnaire</a>
        </div>
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Created By</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @php
                    $ii = 1;
                @endphp
                @foreach($course->questionnaires as $questionnaire)
                    <tr>
                        <td>{{ $ii++ }}</td>
                        <td>{{ $questionnaire->title }}</td>
                        <td>{{ $questionnaire->description }}</td>
                        <td>{{ $questionnaire->coach->name }}</td>
                        <td>{{ $questionnaire->created_at }}</td>
                        <td>
                            @if(Auth::user()->id == $questionnaire->coach_id)
                                <a class="btn btn-info btn-sm" href="{{ route('coach.courses.questionnaires.edit', [$course->id, $questionnaire->id]) }}">Edit</a>
                                <a class="btn --btn-primary btn-sm deleteItem" data-delete-id="{{ $questionnaire->id }}" href="#">Delete</a>
                            @endif
                                <a class="btn btn-info btn-sm" href="{{ route('coach.courses.questionnaires.submissions.index', [$course->id, $questionnaire->id]) }}">Submissions</a>
                                <a class="btn btn-info btn-sm" href="{{ route('coach.courses.questionnaires.quizzes.index', [$course->id, $questionnaire->id]) }}">Quizzes</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{--<ul class="pagination">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
            </ul>--}}
        </div>
    </div>
    <form action="" id="deleteForm" method="POST" >
        {{ method_field('DELETE')}}
        @csrf
    </form>
@endsection

@section('footer-scripts')
    <script>
        var deleteResourceUrl = '{{ url()->current() }}';
        $(document).ready(function () {

            $('.deleteItem').click(function (e) {
                e.preventDefault();
                var deleteId = parseInt($(this).data('deleteId'));

                $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);

                if (confirm('Are you sure?')) {
                    $('#deleteForm').submit();
                }
            });

        });
    </script>
@endsection
