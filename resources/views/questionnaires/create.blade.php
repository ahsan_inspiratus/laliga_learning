@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">Create Questionnaire</h2>
            <a href="{{ route('coach.courses.questionnaires.index', [$course->id]) }}" class="btn btn-primary float-right">Back to Questionnaires</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('coach.courses.questionnaires.store', [$course->id]) }}" method="POST">

                @include('questionnaires._form')

                <input type="submit" value="Save" class="btn btn-primary">
                <a href="{{ route('coach.courses.questionnaires.index', [$course->id]) }}" class="btn btn-warning">Cancel</a>
            </form>
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
            var video_type = $("input[name='type']:checked").val();
            hideShowVideoField(video_type);

            $("input[name='type']").change(function(){
                hideShowVideoField($(this).val());
            });

            function hideShowVideoField(video_type){
                if(video_type == {{ VIDEO_TYPE_UPLOAD }}){
                    $('#video_file').show();
                    $('#video_link').hide();
                } else {
                    $('#video_link').show();
                    $('#video_file').hide();
                }
            }
        });
    </script>
@endsection
