@extends('layouts.app')

@section('content')
@include('partials._header')

    <section class="dashboard-layout bg-grey">
        {{--<header class="dashboard-header bg-red">
            <div class="container">
                <div class="header-content">
                    <h4 class="title">{{ $course->title }}</h4>
                    <h4 class="title">{{ $video->title }}</h4>
                </div>
            </div>
        </header>--}}

        <section class="dashboard-body">
            <div class="container">

                <h2 class="form-title text-center">Training Session</h2>

                <div class="box --video-course-box">
                    @if($video->type == VIDEO_TYPE_UPLOAD)
                        <video controls width="80%">
                            <source src="{{ $video->link }}" type="video/mp4">
                            Sorry, your browser doesn't support embedded videos.
                        </video>
                    @elseif($video->type == VIDEO_TYPE_LIVE_STREAM)
                        <iframe width="100%" height="360px" src="https://stream.laligacademyonline.com:5443/LiveApp/play.html?name={{ $video->link }}&autoplay=true" frameborder="0" allowfullscreen></iframe>
                    @elseif($video->type == VIDEO_TYPE_IMAGE)
                        <figure style="background-image: url('{{ $video->link }}'); height: 400px; background-size: cover; background-position: center;"></figure>
                    @else
                        <iframe width="100%" height="420" src="https://www.youtube.com/embed/{{ $video->link }}" frameborder="0" allowfullscreen>
                        </iframe>
                    @endif


                    <div class="row align-items-center">
                        <div class="col-6">
                            <div id="likes_count" class="likes_count">
                                <button class="btn --btn-small liked-btn">Liked</button>

                                <span>{{ $video->likedBy()->count() }}</span> Likes
                            </div>

                        @if(!Auth::user()->likedVideos()->where('video_id', $video->id)->exists())
                            <button class="btn btn-sm --btn-primary --btn-small like-btn">Like</button>
                        @endif
                        </div>

                        <div class="col-6">
                            <div class="text-right float-right">
                                <form action="{{ route('players.courses.videos.upload', [session('session_id'), $course->id, $video->id]) }}" method="post" enctype="multipart/form-data" class="default-form">
                                    @csrf
                                    <div class="addFileStyle">
                                        <div style="height:0px;overflow:hidden">
                                        <input type="file" name="file" accept="video/*" id="fileInput" onchange="this.form.submit();$('.pre-loading').fadeIn('slow'); $('.pre-loading strong').show();">
                                        </div>
                                        <button type="button" class="btn --btn-small --btn-primary" onclick="fileInput.click();">Upload your Video</button>
                                    </div>
                                </form>

                            @if (isset($_GET['success']))
                                    <div class="fc-yellow">
                                        Your video has been uploaded successfully.
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>



                    <p class="title">{{ $video->title }}</p>

                    <p class="desc">{{ $video->description }}</p>

                    <p class="date"><!-- {{ $video->length }} - --> 22/4/2020</p>

                </div>
            </div>
        </section>

        @include('partials._socialfooter')

    </section>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
            @if(!Auth::user()->likedVideos()->where('video_id', $video->id)->exists())
                $('.liked-btn').hide();
            @endif
            $('.like-btn').click(function () {
               $.post('{{ route('players.courses.videos.like', [session('session_id'), $course->id, $video->id]) }}',
                   {
                       "_token": "{{ csrf_token() }}",
                   },
                   function(data, status){
                       if(data.success) {
                           $('.like-btn').hide();
                           $('.liked-btn').show();

                           var count = parseInt($('#likes_count span').html());
                           $('#likes_count span').html(++count);
                       }
                   });
            });
        });
    </script>
@endsection
