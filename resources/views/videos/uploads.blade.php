@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">{{ $video->title }} - Submission Videos</h2>
        </div>
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>Video</th>
                    <th>Submitted By</th>
                    <th>Uploaded At</th>
                </tr>
                </thead>

                <tbody>
                @php
                    $ii = 1;
                @endphp
                @foreach($video->uploads()->latest()->get() as $upload)
                    <tr>
                        <td>{{ $ii++ }}</td>
                        <td><a href="{{ $upload->link }}" data-fancybox>View</a></td>
                        
                        <td>{{ $upload->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{--<ul class="pagination">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
            </ul>--}}
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
    </script>
@endsection
