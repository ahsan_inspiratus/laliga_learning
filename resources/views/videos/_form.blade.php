@csrf
<div class="control-group">
    <label for="title">Title</label>
    <input type="text" name="title" value="{{ old('title', $video->title ?? '') }}" required class="form-control">
</div>

<div class="control-group">
    <label for="description">Description</label>
    <textarea name="description" class="form-control">{{ old('description', $video->description ?? '') }}</textarea>
</div>


<div class="control-group">
    <label for="type">Type</label>
    <input type="radio" name="type" value="{{ VIDEO_TYPE_UPLOAD }}" checked> Upload
    <input type="radio" name="type" value="{{ VIDEO_TYPE_YOUTUBE }}" @if(old('type', $video->type ?? '') == VIDEO_TYPE_YOUTUBE) checked @endif> Youtube
    <input type="radio" name="type" value="{{ VIDEO_TYPE_LIVE_STREAM }}" @if(old('type', $video->type ?? '') == VIDEO_TYPE_LIVE_STREAM) checked @endif> Live Stream
    <input type="radio" name="type" value="{{ VIDEO_TYPE_IMAGE }}" @if(old('type', $video->type ?? '') == VIDEO_TYPE_IMAGE) checked @endif> Image
</div>

@if(!empty($video) && $video->link)

@endif

<div class="control-group" id="video_file">
    <label for="file">File</label>
    <input type="file" name="file" class="form-control" accept="video/*,image/*" >
</div>

<div class="control-group" id="video_link">
    <label for="link">Link</label>
    <input type="text" name="link" value="{{ old('link', $video->link ?? '') }}" class="form-control">
</div>

<div class="control-group">
    <label for="length">Length</label>
    <input type="text" name="length" value="{{ old('length', $video->length ?? '') }}" class="form-control">
</div>

<div class="control-group">
    <label for="sort_order">Sort Order</label>
    <input type="number" name="sort_order" min="0" value="{{ old('sort_order', $video->sort_order ?? '1') }}" class="form-control">
</div>


