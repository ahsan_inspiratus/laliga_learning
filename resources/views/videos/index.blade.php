@extends('layouts.backend')

@section('content')
    @if(Auth::user()->type == USER_TYPE_COACH)
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">Live Stream & Zoom Meeting Setting</h2>
        </div>
        <div class="card-body">
            <form action="{{ route('coach.courses.update_settings', [$course->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="control-group">
                    <label for="is_live_stream_active">Live Stream</label>
                    <input type="radio" name="is_live_stream_active" required value="0" @if(old('is_live_stream_active', $course->is_live_stream_active ?? '') == 0) checked @endif> Inactive
                    <input type="radio" name="is_live_stream_active" required value="1" @if(old('is_live_stream_active', $course->is_live_stream_active ?? '') == 1) checked @endif> Active
                </div>

                <div class="control-group" id="live_stream_link_div">
                    <label for="live_stream_link">Live Stream Link</label>
                    <input type="text" id="live_stream_link" name="live_stream_link" value="{{ old('live_stream_link', $course->live_stream_link ?? '') }}"  class="form-control">
                </div>

                <div class="control-group">
                    <label for="is_zoom_meeting_active">Zoom Meeting</label>
                    <input type="radio" name="is_zoom_meeting_active" required value="0" @if(old('is_zoom_meeting_active', $course->is_zoom_meeting_active ?? '') == 0) checked @endif> Inactive
                    <input type="radio" name="is_zoom_meeting_active" required value="1" @if(old('is_zoom_meeting_active', $course->is_zoom_meeting_active ?? '') == 1) checked @endif> Active
                </div>

                <div class="control-group" id="zoom_meeting_link_div">
                    <label for="zoom_meeting_link">Zoom Meeting Link</label>
                    <input type="text" id="zoom_meeting_link" name="zoom_meeting_link" value="{{ old('zoom_meeting_link', $course->zoom_meeting_link ?? '') }}"  class="form-control">
                </div>

                <input type="submit" value="Save" class="btn btn-primary">
            </form>
        </div>
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">{{ $course->title }} Videos</h2>
            <a href="{{ route('coach.courses.videos.create', [$course->id]) }}" class="btn btn-primary float-right">+ Add Video</a>
        </div>
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>Title</th>
                    <th>Link</th>
                    <th>Type</th>
                    <th>Length</th>
                    <th>Upload By</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @php
                    $ii = 1;
                @endphp
                @foreach($course->videos as $video)
                    <tr>
                        <td>{{ $ii++ }}</td>
                        <td>{{ $video->title }}</td>
                        <td>{{ $video->link }}</td>
                        <td>{{ in_array($video->type, [VIDEO_TYPE_UPLOAD, VIDEO_TYPE_IMAGE]) ? 'Uploaded' : 'Youtube' }}</td>
                        <td>{{ $video->length }}</td>
                        <td>{{ $video->coach->name }}</td>
                        <td>
                            @if(Auth::user()->id == $video->coach_id)
                                <a class="btn btn-info btn-sm" href="{{ route('coach.courses.videos.edit', [$course->id, $video->id]) }}">Edit</a>
                            @endif
                                <a class="btn btn-info btn-sm" href="{{ route('coach.courses.videos.uploads.index', [$course->id, $video->id]) }}">Submissions</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{--<ul class="pagination">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
            </ul>--}}
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script>
        $(document).ready(function(){
            $("input[name='is_live_stream_active']").change(function(){
                hideShowLiveStreamDiv();
            });
            function hideShowLiveStreamDiv(){
                var is_live_stream_active = $("input[name='is_live_stream_active']:checked").val();
                if(is_live_stream_active == '1'){
                    $('#live_stream_link_div').show();
                } else {
                    $('#live_stream_link_div').hide();
                }
            }

            $("input[name='is_zoom_meeting_active']").change(function(){
                hideShowZoomMeetingDiv();
            });
            function hideShowZoomMeetingDiv(){
                var is_zoom_meeting_active = $("input[name='is_zoom_meeting_active']:checked").val();
                if(is_zoom_meeting_active == '1'){
                    $('#zoom_meeting_link_div').show();
                } else {
                    $('#zoom_meeting_link_div').hide();
                }
            }

            hideShowLiveStreamDiv();
            hideShowZoomMeetingDiv();
        });

    </script>
@endsection
