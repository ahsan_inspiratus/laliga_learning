@extends('layouts.app')

@section('content')
    <section class="registration-layout --fixed align-items-center bg-grey">
        <div class="container">
            <section class="form-container">
                <header class="form-header">
                    <picture>
                        <img src="{{ asset("assets-web/images/laliga-academy-logo.png") }}" alt="" class="m-auto">
                    </picture>
                </header>

                <h2 class="form-title">{{ __('Login') }}</h2>

                <form class="default-form --login-form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="control-group">
                        <input id="email" type="email" class="form-field bg-spgrey @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter Email">
                        @error('email')
                        <span class="error-field" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">
                        <input id="password" type="password" class="form-field bg-spgrey @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter Password">

                        @error('password')
                        <span class="error-field" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="control-group">
                        <div class="checkbox">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="label" style="top: 2px"></label>
                            <p class="text">
                                {{ __('Remember Me') }}
                            </p>
                        </div>
                    </div>

                    <div class="control-group">
                        <input type="submit" value="{{ __('Login') }}" class="btn --btn-submit">
                    </div>
                </form>

                @include('partials._socialfooter')

            </section>
        </div>
    </section>
@endsection
