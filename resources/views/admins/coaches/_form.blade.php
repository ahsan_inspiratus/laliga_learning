@csrf
<div class="control-group">
    <label for="name">Name</label>
    <input type="text" name="name" value="{{ old('name', $coach->name ?? '') }}" required class="form-control">
</div>

<div class="control-group">
    <label for="email">Email</label>
    <input type="email" name="email" value="{{ old('email', $coach->email ?? '') }}" @if(Route::currentRouteName() == 'admin.coaches.create') required @else readonly @endif class="form-control">
</div>

<div class="control-group">
    <label for="password">Password</label>
    <input type="password" name="password" @if(Route::currentRouteName() == 'admin.coaches.create') required @endif class="form-control">
</div>

<div class="control-group">
    <label for="password_confirmation">Confirm Password</label>
    <input type="password" name="password_confirmation" @if(Route::currentRouteName() == 'admin.coaches.create') required @endif class="form-control">
</div>

<div class="control-group">
    <label for="photo">Photo</label>
    <input type="file" name="photo" class="form-control" accept="image/*" >
    @if(!empty($coach->photo))
        <strong>Current Photo</strong><br>
        <img src="{{ Storage::url($coach->photo) }}" alt="" style="max-width: 200px; max-height: 150px">
    @endif
</div>

<div class="control-group">
    <label for="bio">Bio</label>
    <textarea name="bio" class="form-control">{{ old('bio', $coach->bio ?? '') }}</textarea>
</div>
