@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline-block">All Coaches</h2>
            <a href="{{ route('admin.coaches.create') }}" class="btn btn-primary float-right">+ Add Coach</a>
        </div>
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Updated On</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($coaches as $coach)
                    <tr>
                        <td>{{ $coach->id }}</td>
                        <td><img src="{{ Storage::url($coach->photo) }}" alt="" style="max-width: 150px; max-height: 100px;"></td>
                        <td>{{ $coach->name }}</td>
                        <td>{{ $coach->email }}</td>
                        <td>{{ $coach->updated_at }}</td>
                        <td>
                            <a class="btn btn-info btn-sm" href="{{ route('admin.coaches.edit', [$coach->id]) }}">Edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{--<ul class="pagination">--}}
                {{--<li class="page-item active">--}}
                    {{--<a class="page-link" href="#">1</a>--}}
                {{--</li>--}}
                {{--<li class="page-item">--}}
                    {{--<a class="page-link" href="#">2</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        </div>
    </div>
@endsection
