@extends('layouts.backend')

@section('content')
    {{--<div class="row">
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="icon-people bg-primary p-4 font-2xl mr-3"></i>

                    <div>
                        <div class="text-value-sm text-primary">850</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Total Signup</div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="icon-people bg-warning p-4 font-2xl mr-3"></i>

                    <div>
                        <div class="text-value-sm text-warning">80</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Contacted</div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="icon-people bg-success p-4 font-2xl mr-3"></i>

                    <div>
                        <div class="text-value-sm text-success">70</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Converted</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body p-0 d-flex align-items-center">
                    <i class="icon-people bg-danger p-4 font-2xl mr-3"></i>

                    <div>
                        <div class="text-value-sm text-danger">20</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Rejected</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <table class="table table-responsive-md lead-table">
                <thead>
                <tr>
                    <th>S No.</th>
                    <th>User Detail</th>
                    <th>Category</th>
                    <th>Team Member</th>
                    <th>Status</th>
                    <th>Received On</th>
                    <th>Updated On</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>

                <tr>
                    <td>1</td>
                    <td>
                        <strong>Name:</strong> Emad azba <br>
                        <strong>Email:</strong> sarahazba@hotmail.com <br>
                        <strong>Phone:</strong> 0528922633 <br>
                        <strong>Interested In:</strong> Season 3 term 2 (January 2020 - May 2020) <br>
                        <strong>DOB:</strong> 07/04/2006 <br>
                        <span class="btn btn-primary btn-sm collapsed" data-toggle="collapse" href="#collapseEdit">Edit</span>
                    </td>
                    <td>U18</td>
                    <td>Sana</td>
                    <td>
                        <span class="badge badge-info">Follow Up</span>
                    </td>
                    <td>19 Jan 2020 <br> 17:26:00</td>
                    <td>19 Jan 2020 <br> 17:26:00</td>
                    <td>
                        <span class="btn btn-dark btn-sm collapsed" data-toggle="collapse" href="#collapseDetail">Detail</span>
                    </td>
                </tr>

                <tr>
                    <td colspan="8">
                        <div class="fold-content">
                            <div class="collapse show" id="collapseEdit" style="">
                                <table class="table table-responsive-md">
                                    <tr>
                                        <td>
                                            <strong>Name</strong> <br>
                                            <input type="text" class="form-control" value="Emad Azad" />
                                        </td>

                                        <td>
                                            <strong>Email</strong> <br>
                                            <input type="text" class="form-control" value="sarahazba@hotmail.com" />
                                        </td>

                                        <td>
                                            <strong>Phone</strong> <br>
                                            <input type="text" class="form-control" value="0528922633" />
                                        </td>

                                        <td>
                                            <strong>DOB</strong> <br>
                                            <input type="text" class="form-control" value="07/04/2006" />
                                        </td>

                                        <td width="100px">
                                            <strong>Category</strong> <br>
                                            <select class="form-control">
                                                <option value="0">U18</option>
                                            </select>
                                        </td>

                                        <td>
                                            <strong>Interested to Join</strong> <br>
                                            <select class="form-control">
                                                <option value="0">Season 3 term 2 (January 2020 - May 2020)</option>
                                            </select>
                                        </td>

                                        <td style="vertical-align: bottom">
                                            <button class="btn btn-sm btn-success" type="submit">Update</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="collapse" id="collapseDetail" style="">
                                <table class="table">
                                    <tr>
                                        <td width="500px">
                                            <strong>Comment</strong> <br>
                                            <strong>(14/10/2019 17:56:55):</strong> They just moved from UK. But on Sharjah border they will try one day to see and will confirm
                                            <input type="text" class="form-control" />
                                        </td>

                                        <td>
                                            <strong>Team Member</strong> <br>
                                            Sana
                                            <select class="form-control">
                                                <option value="0">Select</option>
                                            </select>
                                        </td>

                                        <td>
                                            <strong>Status</strong> <br>
                                            Follow Up
                                            <select name="status" class="form-control" id="status">
                                                <option value="1">New Enquiry</option>
                                                <option value="2">Contacted</option>
                                                <option value="6" selected="selected">Follow Up</option>
                                                <option value="8">Waiting List</option>
                                                <option value="9">Trial</option>
                                                <option value="3">Call Not Pick</option>
                                                <option value="4">Converted</option>
                                                <option value="5">Rejected</option>
                                                <option value="7">Parked</option>
                                            </select>
                                        </td>

                                        <td>
                                            <strong>Coach Name</strong> <br>
                                            Martin
                                            <select name="status" class="form-control" id="status">
                                                <option value="1">New Enquiry</option>
                                                <option value="2">Contacted</option>
                                                <option value="6" selected="selected">Follow Up</option>
                                                <option value="8">Waiting List</option>
                                                <option value="9">Trial</option>
                                                <option value="3">Call Not Pick</option>
                                                <option value="4">Converted</option>
                                                <option value="5">Rejected</option>
                                                <option value="7">Parked</option>
                                            </select>
                                        </td>

                                        <td>
                                            <strong>Date/ Time</strong> <br>
                                            19 Jan 2020/ 17:26:00
                                            <input class="form-control" id="date-input" type="date" name="date-input" placeholder="date">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <ul class="pagination">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
            </ul>
        </div>
    </div>--}}
@endsection
