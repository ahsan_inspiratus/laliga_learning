<?php
// User Type
define('USER_TYPE_ADMIN', 1);
define('USER_TYPE_PLAYER', 2);
define('USER_TYPE_COACH', 3);


// Video Type
define('VIDEO_TYPE_UPLOAD', 1);
define('VIDEO_TYPE_YOUTUBE', 2);
define('VIDEO_TYPE_LIVE_STREAM', 3);
define('VIDEO_TYPE_IMAGE', 4);


// Status
define('STATUS_ENABLED', 1);
define('STATUS_DISABLED', 0);


// Quiz Type
define('QUIZ_TYPE_MCQS', 1);
define('QUIZ_TYPE_TRUE_FALSE', 2);
define('QUIZ_TYPE_SHORT_QA', 3);


// Media Type
define('MEDIA_TYPE_IMAGE', 1);
define('MEDIA_TYPE_VIDEO', 2);
